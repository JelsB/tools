#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
.. module::ratefunctions
    :platform: Linux
    :synopsis: module illustrating how to document python source code

.. moduleauthor:: Jels Boulangier <jels.boulangier@kuleuven.be>
"""
from argparse import Namespace
from pathlib import Path
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
import figureSettings


dirs = Namespace(krome_models=Path('~/codes/krome_models').expanduser(),
                 krome_build=Path('~/codes/krome/build_nucleation_gfe_FGV'
                                  ).expanduser(),
                 output_rates=Path('./figs_ratefunction')
                 )

files = Namespace(rates='gfe_fixglobvar_rate_dump.dat',
                  info='info.log')


def read_krome_rate_dump(file):
    """Generator for reading in dumped rate file and returning rates and
    temperature in a useful format.

    Args:
        file (Path obj): input file that is output file of function krome_dump_rates()

    Yield:
        rates (dict of float): rates with index as keys
    Yield:
        temps (list of float): temperature

    """
    rates = defaultdict(list)
    temps = []
    temp_key = True
    for row in file:
        if row == ' \n':
            temp_key = True
            continue
        temp, idx,  rate = (x for x in row.split())
        rates[idx].append(float(rate))
        if temp_key:
            temps.append(float(temp))
        temp_key = False
    yield rates
    yield temps


def read_krome_info(file):
    """Generator to read KROME info file.

    Args:
        file (Path obj): input file info.log from KROME

    Yields:
        info (dict of str): chemical reactions

    """
    info = dict()
    reaction_start = False

    for row in file:
        if '#list of reactions (including with multiple limits)' in row:
            reaction_start = True

        if reaction_start:
            if row == '\n':
                break
            idx, reaction = row.split(maxsplit=1)
            info[idx] = reaction.strip()

    yield info


def plot_rate(rate, temp, reaction):
    """Plots one rate in funtion of temperature.

    Args:
        rate (list): rate values
        temp (list): temperature values

    Returns:
        figure
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.semilogy(temp, rate)
    ax.set_ylim(top=max(rate) * 10, bottom=min(rate) / 10)
    ax.set_title(reaction.replace('->', '$->$'))
    plt.savefig( (dirs.output_rates / reaction.replace(" ", "_"))
                .with_suffix('.png'), format='png')
    plt.close()


def plot_all_rates(all_rates, temp, all_reactions):
    """Plots multiple rates by calling :func:`plot_rate`.

    Args:
        all_rates (dict of float): all rate values with index as keys
        temp (list of float): temperature valueList
        all_reactions (dict of str): all reactions with index as keys
    """
    for key, value in all_rates.items():
        # if int(key) < 5:
        reaction = all_reactions[key]
        plot_rate(value, temp, reaction)


def plot_multiple_rates_per_fig(rates, temp, reactions):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ymin = 1e60
    ymax = 1e-60

    for idx, rate in rates.items():

        ax.semilogy(temp, rate, label=reactions[idx].replace('->', '$->$'))
        ymin = min(ymin, min(rate))
        ymax = max(ymax, max(rate))


    ax.set_ylim(top=ymax * 10, bottom=ymin / 10)
    plt.legend()
    plt.show()
    plt.close()


def run_main():
    """Runs main program.
    """
    input_rates = dirs.krome_models / files.rates
    input_info = dirs.krome_build / files.info

    with input_rates.open() as file:
        rates, temp = list(read_krome_rate_dump(file))

    with input_info.open() as file:
        info = list(read_krome_info(file))[0]

    info2 = dict()
    for k, v in info.items():
        if v in info2.values():
            print(v)
        else:
            info2[k] = v

    # plot_all_rates(rates, temp, info)
    # dict_test = {k: v for k, v in rates.items() if float(k)<3}
    # dict_test = dict()
    # dict_test.update('1'=rates['1'])
    # dict_test.update('5'=rates['5'])
    # plot_multiple_rates_per_fig(dict_test, temp, info)

if __name__ == '__main__':
    run_main()
