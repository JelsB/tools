"""This module contains useful snippet for JSON files"""

from pathlib import Path
import json
from astropy.utils.misc import JsonCustomEncoder


def objs_to_json(dict_of_objs, output_file, skip_key=None):
    """Dumps dict of objects to json file.

    Args:
        output_file (Pathlib obj): output file
        skip_key (str): key word that is skipped for output
    """

    json_dict = dict()
    for key, species in dict_of_objs.items():
        # convert object to dict
        json_species = species.__dict__
        if skip_key:
            del json_species[skip_key]
        json_dict[key] = json_species

    with output_file.open('w') as f:
        # use custom Astropy Json encoder
        # e.g. to handle Quantity objects
        json.dump(json_dict, fp=f, cls=JsonCustomEncoder, indent=2)
