"""This module contains useful snippet for csv files"""

import csv
from pathlib import Path



def write_csv(path: Path, columns: iter, header, suffix, delimiter=' '):
    output_file = path.with_suffix(suffix)
    with output_file.open('w') as out:
        out_writer = csv.writer(out, delimiter=delimiter, quotechar='#')
        out_writer.writerow(header)
        out_writer.writerows(columns)
    print('Wrote out {}'.format(output_file.name))
