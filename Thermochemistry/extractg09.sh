#/bin/sh 
fgrep 'SCF D' $1 | tail -1 | awk '{printf "%16.10f ", $5}'
fgrep 'Zero-point correction' $1  | awk '{printf "%10.6f", $3}'
fgrep 'Rotational symmetry number' $1  | awk '{printf "%3i", $4}'
fgrep 'Molecular mass' $1  | awk '{printf "%10.5f\n", $3}'
fgrep 'GHZ' $1 | tail -2 | head -1 | awk '{printf "%11.6f %11.6f %11.6f\n", $4, $5, $6}'
fgrep Frequencies $1 | awk '{printf "%11.5f %11.5f %11.5f\n", $3, $4, $5}'
fgrep 'has atomic number' $1 | tail -1 | awk '{print " ", $2-2}'
