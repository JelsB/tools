
class LiteratureAtom:
    """Define an atom.

    NOTE that all functions/attributes are at reference value of 0.1 MPa
    and T=298.15 K.
    """

    def __init__(self, name, energy, temperature: List[float],
                 entropy: List[float], diff_enthalpy: List[float],
                 formation_enthalpy, amount_in_mole=1):
        """Initialise the atom object."""
        self.name = name
        self.mole = amount_in_mole
        self.energy = hartree_to_joule(energy[0], self.mole)
        self.raw_temperature = np.array(temperature)
        self.raw_entropy = np.array(entropy)
        self.raw_diff_enthalpy = np.array(diff_enthalpy)
        self.ref_formation_enthalpy = formation_enthalpy

    def total_entropy(self, Tgas):
        "Interpolate entropy from given list."
        return (griddata(self.raw_temperature, self.raw_entropy, Tgas)
                * u.J/(u.K * u.mol))

    def diff_enthalpy(self, Tgas):
        "Interpolate enthalpy difference (wrt ref) from given list."
        return griddata(self.raw_temperature,
                        self.raw_diff_enthalpy, Tgas)*u.kJ/u.mol

    def __repr__(self):
        return "{class_}({name})".format(
                class_=self.__class__.__name__, **vars(self))


class CombinedSpecies:
    """Define a molecule ans its atomic constituents."""

    def __init__(self, molecule: Molecule, atoms: List[LiteratureAtom],
                 counter: Counter):
        self.molecule = molecule
        self.atoms = atoms
        self.atom_counter = counter

    @property
    def atomization_energy(self):
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += self.atom_counter[atm.name] * atm.energy

        return (atomic_part - self.molecule.energy
                - self.molecule.zero_point_energy)

    @property
    def ref_molecular_formation_enthalpy(self):
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += (self.atom_counter[atm.name]
                            * atm.ref_formation_enthalpy)

        return atomic_part - self.atomization_energy

    def formation_enthalpy(self, Tgas):
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += (self.atom_counter[atm.name]
                            * atm.diff_enthalpy(Tgas))

        return (self.ref_molecular_formation_enthalpy
                + self.molecule.total_enthalpy(Tgas)
                - self.molecule.total_enthalpy(ref_temperature)
                - atomic_part)

    # def formation_gibbs_free_energy(self, Tgas):
    #     atomic_entropy = 0
    #     for atm in self.atoms:
    #         atomic_entropy += (self.atom_counter[atm.name]
    #                            * atm.total_entropy(Tgas))
    #
    #     return (self.formation_enthalpy(Tgas)
    #             - Tgas * (self.molecule.std_total_entropy(Tgas)
    #                       - atomic_entropy)
    #             )
    def formation_gibbs_free_energy(self, Tgas):
        # print(self.molecule.zero_point_energy, self.molecule.total_enthalpy(ref_temperature))
        molecule_part = (self.molecule.total_enthalpy(Tgas)
                        - self.molecule.total_enthalpy(ref_temperature)
                        + self.molecule.energy
                        + self.molecule.zero_point_energy
                        - Tgas * self.molecule.std_total_entropy(Tgas)
                        )
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += (self.atom_counter[atm.name]
                            * (atm.diff_enthalpy(Tgas)
                               + atm.energy
                               - atm.ref_formation_enthalpy
                               - Tgas * atm.total_entropy(Tgas)
                               )
                            )
        return molecule_part - atomic_part

    def __repr__(self):
        return "{class_}({molecule},{atoms})".format(
                class_=self.__class__.__name__, **vars(self))

def get_atom_data(name, path: Path):
    temperature = []
    entropy = []
    diff_enthalpy = []
    ref_formation_enthalpy = []
    internal_energy = []

    with path.open('r') as f_in:
        reader = csv.reader(f_in, delimiter='\t', )
        # skip header
        next(reader)
        for row in reader:
            temperature.append(float(row[0]))
            entropy.append(float(row[2]))
            diff_enthalpy.append(float(row[4]))
            if row[0] == reference_temperature:
                ref_formation_enthalpy.append(float(row[5]))

    with atomic_internal_energy_file.open('r') as f_in:
        reader = csv.reader(f_in, delimiter=',', )
        # skip header
        next(reader)
        for row in reader:
            if row[0] == name:
                internal_energy.append(float(row[1]))

    return (temperature, entropy, diff_enthalpy, ref_formation_enthalpy,
            internal_energy)

def make_atom(name):
    atm_patern = re.compile(name)
    atomfile = list(filter(lambda item_file:
                    path_atom_filter(item_file, atm_patern),
                    atom_files))[0]

    (temperature, entropy, diff_enthalpy, ref_formation_enthalpy,
     internal_energy) = get_atom_data(name, atomfile)

    # convert to quantity objects with units
    temperature *= u.K
    entropy *= u.J/(u.K * u.mol)
    diff_enthalpy *= u.kJ / u.mol
    ref_formation_enthalpy *= u.kJ / u.mol

    return Atom(name, internal_energy, temperature, entropy,
                diff_enthalpy, ref_formation_enthalpy)
