import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
from astropy.constants import h, k_B, R, N_A
from astropy.units import cds
from typing import List
from scipy.interpolate import griddata
from atomic_info import get_species_mass, count_constituent_atoms, is_atom
from csv_snippets import write_csv
from pandas_snippet import rename_dataframe_header
import pandas as pd
from collections import Counter
from json_snippets import objs_to_json


pi = 3.1415
Hartree = (2.*u.Ry).to(u.J)


def hartree_to_joule(energy, mole):
    """Convert internal energy to joule

    Input is in Hartree per particle.
    TODO: make this an Astropy conversion
    """
    return energy * Hartree * mole * N_A


def freq_to_temp(freqs):
    """Convert fequency to temperature in K.

    Input freqs is in cm^-1
    """
    return freqs.to(1./u.s, equivalencies=u.spectral()) * h/k_B


def freq_to_energy(freqs):
    """Convert fequency to energy in J.

    Input freqs is in cm^-1
    """
    return freqs.to(1./u.s, equivalencies=u.spectral()) * h


class Species:
    """
    Define a chemical species.

    This can be any type of molecule or an atom.
    """

    def __init__(self, name, mass, energy, elec_energies, elec_degeneracies,
                 internal_partition: pd.DataFrame):
        "Initialise the species object"
        self.name = name
        self.mass = self.set_mass(mass)
        #  energy per particle in Joule
        self.energy = energy * Hartree
        self.elec_energies = elec_energies
        self.elec_degeneracies = elec_degeneracies
        self.internal_partition_df = internal_partition

    def __repr__(self):
        return "{class_}({name})".format(
                class_=self.__class__.__name__, **vars(self))

    def set_mass(self, mass):
        if mass:
            return mass.to(u.kg)
        else:
            return get_species_mass(count_constituent_atoms(self.name))

    def translational_partition(self, Tgas, pressure=1*u.bar):
        """Calculate translational partition function.

        Default is at standard pressure of 1 bar (1e5 Pa)
        """
        # decompose resolves the units make them cancel
        return ((2. * self.mass * pi * k_B * Tgas / h**2)**(3./2.)
                * k_B * Tgas * N_A.value / pressure.to(u.Pa)).decompose()

    def electronic_partition(self, Tgas):
        """Calculate electronic energy.

        Z_e = SUM_0^N g_i exp(-E_i/(k_B*Tgas)) becomes
        Under the assumption that the Nth electronic excitation energy
        is much greater than k_B*Tgas.
        All energy levels are w.r.t. the lowest one, so E_0=0.
        """
        result = 0
        for g, E in zip(self.elec_degeneracies, self.elec_energies):
            result += g * np.exp(-E/(k_B*Tgas))

        return result

    def internal_partition(self, Tgas):
        "Interpolate internal partition function from given dataframe."
        return (griddata(self.internal_partition_df['temperature'].values,
                         self.internal_partition_df['internal_partition'].values,
                         Tgas))

    def total_partition(self, Tgas):
        "Return total partition function."
        # use tabulated internal partition function if available
        if self.internal_partition_df.empty:
            return (self.translational_partition(Tgas)
                    * self.electronic_partition(Tgas))
        else:
            return (self.translational_partition(Tgas)
                    * self.internal_partition(Tgas))

    # def translational_entropy(self, Tgas):
    #     """Calculate the translational entropy.
    #
    #     At a pressure of 1 bar.
    #     """
    #     return k_B*(np.log(self.translational_partition(Tgas)) + 3./2.)
    #
    # def electronic_entropy(self, Tgas):
    #  #    """Calculate electronic entropy.
    #  #
    #  #    Under the assumption that the first electronic excitation energy
    #  #    is much greater than k_B*Tgas
    #  #    """
    #     return k_B * np.log(self.electronic_partition(Tgas))
    #
    # def total_entropy_1_particle(self, Tgas):
    #     return self.translational_entropy(Tgas) + self.electronic_entropy(Tgas)
    #
    #
    # def total_entropy_N_particles(self, Tgas, N=N_A.value):
    #     return (N*self.total_entropy_1(Tgas)
    #             - N*k_B*np.log(N)
    #             + N*k_B)
    #
    # def translational_energy(self, Tgas):
    #     """Calculate translational internal energy."""
    #     return 3./2. * k_B * Tgas
    #
    # def total_energy(self, Tgas):
    #     """Calculate total internal energy."""
    #     return (self.translational_energy(Tgas))
    #
    # def total_enthalpy(self, Tgas):
    #     """Calculate total total enthalpy."""
    #     return self.total_energy(Tgas) + k_B * Tgas

    def gibbs_free_energy_one_particle(self, Tgas):
        "Return total gibbs free energy valid for one paricle"
        return (self.energy + k_B*Tgas
                - k_B*Tgas*np.log(self.total_partition(Tgas))
                )

    def gibbs_free_energy_largeN_particles(self, Tgas, N=N_A.value):
        """Return total gibbs free energy valid forN particles.

        Note that this uses Stirling's approximation and is therefore
        only valid for N >> 1.
        Default is 1 mole of paricles.
        """
        if N < 1e6:
            print("N is too small to use the Gibbs free energy approximation"
                  " for large numbers")
            return

        else:
            return (N * self.energy
                    - N*k_B*Tgas*np.log(self.total_partition(Tgas))
                    + N*k_B*Tgas*np.log(N)
                    )


class Atom(Species):
    """Define an atom"""
    pass


class Molecule(Species):
    """Define a chemical molecule."""

    def __init__(self, name, mass, energy, elec_energies, elec_degeneracies,
                 internal_partition: pd.DataFrame, rot_temperatures,
                 rot_symmetry, vib_temperatures):
        """Initialise the molecule object."""
        super().__init__(name, mass, energy, elec_energies, elec_degeneracies,
                         internal_partition)
        self.rot_temperatures = rot_temperatures
        self.rot_symm = rot_symmetry[0]
        self.vib_temperatures = vib_temperatures

    @property
    def multiplied_rot_temp(self):
        """Multiply rotational temperatures."""
        result = 1

        for value in self.rot_temperatures:
            result *= value
        return result

    def vibrational_partition(self, Tgas):
        """Calculate vibrational partition function."""
        result = 1

        for v_temp in self.vib_temperatures:
            x = v_temp / Tgas
            result *= np.exp(-0.5*x) / (1 - np.exp(-x))
        return result

    def rotational_partition(self, Tgas):
        """Calculate rotational partition function."""
        return ((pi * Tgas**3 / self.multiplied_rot_temp)**(0.5)
                / self.rot_symm)

    def total_partition(self, Tgas):
        "Return total partition function."

        # use tabulated internal partition function if available
        if self.internal_partition_df.empty:
            return (self.translational_partition(Tgas)
                    * self.rotational_partition(Tgas)
                    * self.vibrational_partition(Tgas)
                    * self.electronic_partition(Tgas))
        else:
            return (self.translational_partition(Tgas)
                    * self.internal_partition(Tgas))

    # TODO: update everything below to make it compatible with new class input
    # def rotational_entropy(self, Tgas):
    #     """Calculate the translational entropy."""
    #     return R * (np.log(self.rotational_partition(Tgas)) + 3./2.)
    #
    # def vibrational_entropy(self, Tgas):
    #     """Calculate the translational entropy."""
    #     result = 0
    #
    #     for v_temp in self.vib_temperatures:
    #         x = v_temp / Tgas
    #         result += x / (np.exp(x) - 1.) - np.log(1. - np.exp(-x))
    #
    #     return R * result
    #
    # def std_translation_entropy(self, Tgas):
    #     """Calculate the translational entropy.
    #
    #     At a pressure of 1 bar.
    #     """
    #     return R*(np.log(self.std_translational_partition(Tgas)) + 1. + 3./2.)
    #
    # def translation_entropy(self, Tgas, pressure):
    #     """Calculate the translational entropy.
    #
    #     At any given pressure.
    #     """
    #     return (self.std_translation_entropy(Tgas)
    #             + R * np.log(u.bar / pressure))
    #
    # def electronic_entropy(self, Tgas):
    #     """Calculate electronic entropy.
    #
    #     Under the assumption that the first electronic excitation energy
    #     is much greater than k_B*Tgas
    #     """
    #     if (len(self.electron_energy_levels) > 1 or
    #             len(self.electron_spin_degens) > 1):
    #         print('''Electronic entropy is not defined for multiple
    #                 energy levels''')
    #         return
    #     else:
    #         return R * np.log(self.electron_spin_degens[0])
    #
    # def std_total_entropy(self, Tgas):
    #     """Calculate the total entropy of the species at 1 bar."""
    #     return (self.std_translation_entropy(Tgas)
    #             + self.rotational_entropy(Tgas)
    #             + self.vibrational_entropy(Tgas)
    #             + self.electronic_entropy(Tgas))
    #
    # def total_entropy(self, Tgas, pressure):
    #     """Calculate the total entropy of the species."""
    #     return (self.translation_entropy(Tgas, pressure)
    #             + self.rotational_entropy(Tgas)
    #             + self.vibrational_entropy(Tgas)
    #             + self.electronic_entropy(Tgas))
    #
    # def translational_energy(self, Tgas):
    #     """Calculate translational internal energy."""
    #     return 3./2. * R * Tgas
    #
    # def rotational_energy(self, Tgas):
    #     """Calculate rotational internal energy."""
    #     return 3./2. * R * Tgas
    #
    # def vibrational_energy(self, Tgas):
    #     """Calculate vibration internal energy."""
    #     result = 0
    #
    #     for v_temp in self.vib_temperatures:
    #         try:
    #             result += v_temp * (0.5 + 1./(np.exp(v_temp / Tgas) - 1))
    #         # Exception when division by zero when Tgas is zero.
    #         except(RuntimeWarning):
    #             result += v_temp * 0.5
    #
    #     return R * result
    #
    # def electronic_energy(self, Tgas):
    #     """Calculate electronic energy.
    #
    #     Under the assumption that the first electronic excitation energy
    #     is much greater than k_B*Tgas,
    #     this contribution is zero.
    #     """
    #     # if (len(self.elec_energies) > 1 or
    #     #         len(self.elec_degeneracies) > 1):
    #     #     print('''Electronic energy is not defined for multiple
    #     #             energy levels''')
    #     #     return
    #     # else:
    #     return 0.*u.J/u.mol
    #
    # def total_energy(self, Tgas):
    #     """Calculate total internal energy."""
    #     return (self.translational_energy(Tgas)
    #             + self.rotational_energy(Tgas)
    #             + self.vibrational_energy(Tgas)
    #             + self.electronic_energy(Tgas))
    #
    # def total_enthalpy(self, Tgas):
    #     """Calculate total total enthalpy."""
    #     return self.total_energy(Tgas) + R * Tgas
    #
    # def std_total_gibbs_free_energy(self, Tgas):
    #     """Calculate Gibbs free energy at 1 bar."""
    #     # return (self.total_enthalpy(Tgas)
    #     #         + self.energy
    #     #         - Tgas * self.std_total_entropy(Tgas)).to(u.kJ/u.mol)
    #
    #     return (R * Tgas - R * Tgas * np.log(self.total_partition(Tgas))
    #             + self.energy).to(u.kJ/u.mol)
    #
    # def total_gibbs_free_energy(self, Tgas, pressure):
    #     """Calculate Gibbs free energy."""
    #     return (self.total_enthalpy(Tgas)
    #             + self.energy
    #             - Tgas * self.total_entropy(Tgas, pressure))


class LinearMolecule(Molecule):
    """Define a linear molecule."""
    def rotational_partition(self, Tgas):
        """Calculate rotational partition function."""
        return Tgas / (self.rot_symm * self.rot_temperatures[0])

    def rotational_entropy(self, Tgas):
        """Calculate the translational entropy."""
        return R * (np.log(self.rotational_partition(Tgas)) + 1.)

    def rotational_energy(self, Tgas):
        """Calculate rotational internal energy."""
        return R * Tgas


class LiteratureAtom:
    """Define an atom.

    All input data comes from themochemical tables that do not provide
    fundamental degrees of freedom (rotational constants, vibrational
    frequencies) or partition functions. This data can however be used
    to reverse engineer desired properties like Gibbe free energy.

    NOTE that all functions/attributes are at reference value of 0.1 MPa
    """
    def __init__(self, name, diff_enthalpy, energy):
        self.name = name
        self.diff_enthalpy = diff_enthalpy*u.kJ/u.mol
        self.energy = (energy * Hartree * N_A).to(u.kJ/u.mol)

    def __repr__(self):
        return "{class_}({name})".format(
                class_=self.__class__.__name__, **vars(self))


class LiteratureMolecule:
    """Define a molecule.

    All input data comes from themochemical tables that do not provide
    fundamental degrees of freedom (rotational constants, vibrational
    frequencies) or partition functions. This data can however be used
    to reverse engineer desired properties like Gibbe free energy.

    NOTE that all functions/attributes are at reference value of 0.1 MPa
    """

    def __init__(self, name, temperature, entropy, diff_enthalpy,
                 formation_enthalpy_0, formation_enthalpy_298, energy, E_zpve):
        """Initialise the atom object."""
        self.name = name
        self.raw_temperature = np.array(temperature)*u.K
        self.raw_entropy = np.array(entropy)
        self.raw_diff_enthalpy = np.array(diff_enthalpy)
        self.ref_formation_enthalpy_0 = formation_enthalpy_0*u.kJ/u.mol
        self.ref_formation_enthalpy_298 = formation_enthalpy_298*u.kJ/u.mol
        self.zero_point_vibrational_energy = (E_zpve*N_A*u.eV).to(u.kJ/u.mol)
        self.energy = (energy * Hartree * N_A).to(u.kJ/u.mol)

    def __repr__(self):
        return "{class_}({name})".format(
                class_=self.__class__.__name__, **vars(self))

    def total_entropy(self, Tgas):
        "Interpolate entropy from given list."
        return (griddata(self.raw_temperature, self.raw_entropy, Tgas)
                * u.J/(u.K * u.mol))

    def diff_enthalpy(self, Tgas):
        "Interpolate enthalpy difference (wrt ref) from given list."
        # print(self.raw_diff_enthalpy)
        return griddata(self.raw_temperature,
                        self.raw_diff_enthalpy, Tgas)*u.kJ/u.mol


class LiteratureSpecies:
    """Define a molecule ans its atomic constituents."""

    def __init__(self, molecule: LinearMolecule, atoms: List[LiteratureAtom],
                 counter: Counter):
        self.molecule = molecule
        self.atoms = atoms
        self.atom_counter = counter

    @property
    def enthalpy_298(self):
        atomic_part = 0
        for atm in self.atoms:
            atomic_part += self.atom_counter[atm.name] * atm.diff_enthalpy

        return (self.molecule.ref_formation_enthalpy_298
                - self.molecule.ref_formation_enthalpy_0
                + self.molecule.zero_point_vibrational_energy
                + atomic_part
                )

    def gibbs_free_energy(self, Tgas):
        return (self.molecule.diff_enthalpy(Tgas)
                + self.enthalpy_298
                + self.molecule.energy
                - Tgas * self.molecule.total_entropy(Tgas)
                ).to(u.J/u.mol)


if __name__ == "__main__":
    import glob
    from pathlib import Path
    from argparse import Namespace
    from pprint import pprint
    import re
    import csv
    from collections import defaultdict

    # needed directories
    options = Namespace(
        thermochem_dir='/home/jels/tools/Thermochemistry/data',
        atom_dir='/home/jels/tools/Thermochemistry/data',
        output_dir='/home/jels/tools/Thermochemistry/out_test'
        # atom_energy_file='/home/jels/tools/Thermochemistry/data'
        #                 + '/atoms/atomic_internal_energy.dat'
        )
    # RegEx paterns
    # patern of XnYmZk... and Xn type of molecules
    molecule_patern = re.compile(r'([A-Z]+[a-z]*\d*){2,}|[A-Z]+\d+')
    atom_patern = re.compile(r'[A-Z]+[a-z]*')
    species_pattern = re.compile(molecule_patern.pattern
                                 + '|' + atom_patern.pattern)

    # search keys for gaussian output files (*.dft)
    rot_symmetry_key = 'Rotational symmetry number'
    vib_frequencies_key = 'Frequencies --'
    rot_temperatures_key = 'Rotational temperature'
    elec_degeneracy_key = 'Multiplicity = '
    molecule_mass_key = 'Molecular mass'
    energy_key = "SCF Done:"
    zero_point_energy_key = "Zero-point correction"
    # reference_temperature = "0"

    def path_molecule_filter(path: Path, patern=molecule_patern):
        """Filter path objects for molecule patern files."""
        return patern.match(path.name)

    def path_atom_filter(path: Path, patern=atom_patern):
        """Filter path objects for atom patern files."""
        return patern.match(path.name)

    def parse_gaussian_data(source_iter):
        rot_symmetry = []
        vib_frequencies = []
        rot_temperatures = []
        elec_degeneracy = []
        molecule_mass = []
        energy = []

        for row in source_iter:
            if energy_key in row and not energy:
                energy = [clean_enery_input_string(row)]

            elif rot_symmetry_key in row:
                rot_symmetry = clean_input_string(row, rot_symmetry_key)

            elif vib_frequencies_key in row:
                cleaned_row = clean_input_string(row, vib_frequencies_key)
                for i in cleaned_row:
                    vib_frequencies.append(i)

            elif rot_temperatures_key in row:
                rot_temperatures = clean_input_string(
                                        row, rot_temperatures_key)

            elif elec_degeneracy_key in row:
                if not elec_degeneracy:
                    elec_degeneracy = clean_input_string(
                                        row, elec_degeneracy_key)

            elif molecule_mass_key in row:
                molecule_mass = [clean_input_string(row, molecule_mass_key)[0]]

        yield energy
        yield rot_symmetry
        yield vib_frequencies
        yield rot_temperatures
        yield elec_degeneracy
        yield molecule_mass

    def get_species_data(input_file: Path):
        df = pd.read_csv(input_file)
        new_header = ['energy', 'vib_freq', 'rot_freq', 'rot_symm',
                      'elec_freq', 'elec_degen']
        rename_dataframe_header(df, new_header)

        energy = df['energy'].dropna().values
        vib_frequencies = df['vib_freq'].dropna().values
        rot_frequencies = df['rot_freq'].dropna().values
        rot_symmetry = df['rot_symm'].dropna().values
        elec_frequencies = df['elec_freq'].dropna().values
        elec_degeneracy = df['elec_degen'].dropna().values

        return (energy, vib_frequencies, rot_frequencies, rot_symmetry,
                elec_frequencies, elec_degeneracy)

    def get_internal_partition_function(input_file: Path):
        internal_partition = pd.read_csv(input_file)

        new_header = ['temperature', 'internal_partition']
        rename_dataframe_header(internal_partition, new_header)
        return internal_partition

    def get_thermochem_data(input_file: Path, species_name):
        df = pd.read_csv(input_file)
        new_header = ['temp', 'entropy', 'diff_enthalpy', 'formation_enthalpy',
                      'energy', 'E_zpve']
        rename_dataframe_header(df, new_header)

        if is_atom(species_name):
            diff_enthalpy = df['diff_enthalpy'].values[0]
            energy = df['energy'].values[0]
            return diff_enthalpy, energy

        else:
            temperature = df['temp'].values
            entropy = df['entropy'].values
            diff_enthalpy = df['diff_enthalpy'].values
            formation_enthalpy_0 = df.loc[df['temp'] == 0]['formation_enthalpy'].values[0]
            formation_enthalpy_298 = df.loc[df['temp'] == 298.15]['formation_enthalpy'].values[0]
            energy = df['energy'].values[0]
            E_zpve = df['E_zpve'].values[0]
            return (temperature, entropy, diff_enthalpy, formation_enthalpy_0,
                    formation_enthalpy_298, energy, E_zpve)

    def make_species(name, filenames):
        literature_data = []
        gaussian_data = []
        thermochem_data = None
        rot_temperatures = []
        partition_data = pd.DataFrame()
        use_literature_energies = False
        species_mass = None

        atom_counter = count_constituent_atoms(name)

        for filename in filenames:
            extension = filename.suffix
            with filename.open('r') as f:
                if extension == '.dat':
                    literature_data = get_species_data(f)

                elif extension == '.dft':
                    gaussian_data = list_of_list_to_float(
                                    list(parse_gaussian_data(f)))

                elif extension == '.pf':
                    partition_data = get_internal_partition_function(f)

                elif extension == '.therm':
                    thermochem_data = get_thermochem_data(f, name)

                else:
                    print('{} is not yet usuable.'.format(f.name))

        if gaussian_data and not partition_data.empty:
            print('Gaussian and literature parition function data should'
                  'not be mixed')

        if literature_data:
            (energy, vib_frequencies, rot_frequencies, rot_symmetry,
             lit_elec_frequencies, lit_elec_degeneracy) = literature_data

            vib_temperatures = freq_to_temp(vib_frequencies/u.cm)
            rot_temperatures = freq_to_temp(rot_frequencies/u.cm)
            lit_elec_energies = freq_to_energy(lit_elec_frequencies/u.cm)

            elec_degeneracies = lit_elec_degeneracy
            elec_energies = lit_elec_energies

            if len(lit_elec_energies) > 1:
                use_literature_energies = True

        if gaussian_data:
            (energy, rot_symmetry, vib_frequencies, rot_temperatures,
             gaussian_elec_degeneracy, species_mass) = gaussian_data

            # convert one value list to value
            energy = energy[0]
            species_mass = species_mass[0]
            # convert to quantity objects with units
            rot_temperatures *= u.K
            vib_temperatures = freq_to_temp(vib_frequencies/u.cm)
            species_mass *= u.u  # atomic mass

            elec_energies = [0]*u.J
            elec_degeneracies = gaussian_elec_degeneracy

            if use_literature_energies:
                elec_degeneracies = lit_elec_degeneracy
                elec_energies = lit_elec_energies

        # make literature species objects
        if thermochem_data:
            if is_atom(name):
                diff_enthalpy, energy_therm = thermochem_data
                species = LiteratureAtom(name, diff_enthalpy, energy)

            else:
                (temperature, entropy, diff_enthalpy, formation_enthalpy_0,
                 formation_enthalpy_298, energy_therm, E_zpve) = thermochem_data

                species = LiteratureMolecule(name, temperature, entropy,
                                             diff_enthalpy,
                                             formation_enthalpy_0,
                                             formation_enthalpy_298,
                                             energy, E_zpve)
            # store object is separate dict
            dict_of_lit_atom_mol[name] = species

        # make species objects
        if len(rot_temperatures) > 1:
            species = Molecule(name, species_mass, energy, elec_energies,
                               elec_degeneracies, partition_data,
                               rot_temperatures, rot_symmetry,
                               vib_temperatures)

        elif len(rot_temperatures) == 1:
            species = LinearMolecule(name, species_mass, energy, elec_energies,
                                     elec_degeneracies, partition_data,
                                     rot_temperatures, rot_symmetry,
                                     vib_temperatures)

        elif is_atom(name):
            species = Atom(name, species_mass, energy, elec_energies,
                           elec_degeneracies, partition_data)

        elif not thermochem_data:
            print('Something went wrong when making species {}'.format(name))

        return species

    def write_out_gibbs_free_energy(species, out_path):

        krome_name = species.name.upper()
        gibbs_free_energy = species.gibbs_free_energy_largeN_particles(
                                    gas_temperature).to(u.kJ)/u.mol
        out_file = out_path.joinpath(krome_name)
        out_header = ['temperature ({})'.format(gas_temperature.unit),
                      'Gibbs free energy ({})'.format(
                              gibbs_free_energy.unit)]
        file_suffix = '.gfe'

        out_columns = zip(gas_temperature.value, gibbs_free_energy.value)
        write_csv(out_file, out_columns, out_header, file_suffix)

    def space_to_comma(string):
        return re.sub(r'\s+', ',', string)

    def clean_input_string(string, remove_key):
        return space_to_comma(re.sub(r'.*' + remove_key + r'\D*', '', string)
                              .strip()).split(',')

    def clean_enery_input_string(row):
        return row.split('=')[1].strip().split(' ')[0]

    def list_to_float(list):
        return [float(i) for i in list]

    def list_of_list_to_float(list_of_list):
        return [list_to_float(i) for i in list_of_list]

    gas_temperature = list(range(100, 3050, 10))*u.K
    # ref_temperature = float(reference_temperature) * u.K
    # gas_temperature = [1000]*u.K
    gas_pressure = []

    molecule_path = Path(options.thermochem_dir)
    atom_path = Path(options.atom_dir)
    out_path = Path(options.output_dir)

    gaussian_data_files = list(molecule_path.glob("*.dft"))
    # janaf_files = list(atom_path.glob("*.janaf"))
    literature_species_data_files = list(atom_path.glob("*.dat"))
    partition_function_files = list(molecule_path.glob("*.pf"))
    thermochemical_files = list(molecule_path.glob("*.therm"))

    # atomic_internal_energy_file = Path(options.atom_energy_file)
    # atom_files = list(filter(path_atom_filter, janaf_files))
    molecule_files = list(filter(path_molecule_filter, gaussian_data_files))
    species_data_files = (literature_species_data_files + gaussian_data_files
                          + partition_function_files + thermochemical_files)

    species_file_dict = defaultdict(list)
    dict_of_species = {}
    dict_of_lit_atom_mol = {}
    dict_of_lit_species = {}

    for sp_file in species_data_files:
        name = re.match(species_pattern, sp_file.stem).group()
        species_file_dict[name].append(sp_file)

    for sp, files in species_file_dict.items():
        species = make_species(sp, files)
        dict_of_species[sp] = species

    for key, species in dict_of_species.items():
        write_out_gibbs_free_energy(species, out_path)

    # under construction
    # write objects to json file
    objs_to_json(dict_of_species, Path('test.json'),
                 skip_key='internal_partition_df')


# DEBUG stuff below
        # if isinstance(species, Species) and 'Ti' in sp:
            # print(sp, species.total_enthalpy(gas_temperature).to(u.kJ/u.mol))
            # print(sp, species.gibbs_free_energy_largeN_particles(gas_temperature))

    # for sp, sp_obj in dict_of_lit_atom_mol.items():
    #     if not is_atom(sp):
    #         atom_counter = count_constituent_atoms(name)
    #         # list with atom objects that present in the molecule
    #         atom_objs = [dict_of_lit_atom_mol[atm] for atm in atom_counter]
    #         sp_lit = LiteratureSpecies(sp_obj, atom_objs, atom_counter)
    #         dict_of_lit_species[sp] = sp_lit
            # print("LIT", sp, sp_lit.gibbs_free_energy(gas_temperature))
            # print(sp, sp_obj.energy)
            # print(sp, sp_lit.enthalpy_298)
    # Tgas = [1000]*u.K
    # test_species = 'Mg'
    # print(species_file_dict[test_species])
    # species = make_species(test_species, species_file_dict[test_species])
    # # write_out_gibbs_free_energy(species, out_path)
    # print(species.gibbs_free_energy_largeN_particles(Tgas))
    # print(species.total_partition(Tgas))
    # print(species.translational_partition(Tgas))
    # print(species.translational_entropy(Tgas)*N_A)
    # print(species.total_entropy_N(Tgas), 'tot S')
    # print(species.mass)
    # print(1*u.u.to(u.kg))
    # print(species.rotational_partition(Tgas))

    # print(dict_of_species[test_species].gibbs_free_energy_largeN_particles(gas_temperature))



    # Test for MgO at 1000K and 1e5 Pa
    # spreadsheet gives -7.2278777e5 kJ (per mole)
    # ours gives -7.22457071e+08 J (per mole)
    # difference is due to slightly different input values of rot and vib temperatures
    # spreadsheet: vib: freq_to_temp(814.852/u.cm) = 1172.3906 K
    #              rot: 17.340037 GHz -> 0.83219 K
    # here:        vib: 1134.3795 K
    #              rot: 0.835989 K
    # When we also include electronic levels Z_e = 1.38420719 instead of 1
    # slightly changing the GFE to -7.22459774e+08 J (0.0003 per cent change)
    # print(dict_of_species['MgO'].rot_temperatures)
    # print(dict_of_species['MgO'].electronic_partition(gas_temperature))
    # print(dict_of_species['MgO'].gibbs_free_energy_largeN_particles(gas_temperature))
    # print(dict_of_species['MgO'].gibbs_free_energy_one_particle(gas_temperature))
