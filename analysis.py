import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import re
import sys
import glob
import os
import figureSettings

############## Constants #############
Msol = 1.989e30		# kg
AU = 1.4960e11		# m
kboltz = 1.380648e-23
Grav = 6.674e-11		#
mh = 1.67262158e-27  # kg
############## Conversion #############
AU2Rsol = 215.  # Rsol
Rsol2AU = 1. / AU2Rsol  # AU
############# Line styles ##############
#lineColors = ["b","r","k","grey","k"]
# lineColors = ["#e41a1c","#377eb8","#4daf4a","#984ea3","#ff7f00","#ffff33","#a65628","#f781bf"]
# lineColors = ["#332288", "#88CCEE", "#44AA99", "#117733", "#999933", "#DDCC77", "#CC6677", "#882255", "#AA4499"]
# lineColors = ["#332288", "#117733", "#882255"]
lineColors = ['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c']
# lineColors = ['#1b9e77','#d95f02','#7570b3']
#['#eff3ff','#bdd7e7','#6baed6','#2171b5']
# lineColors= ['#deebf7','#9ecae1','#3182bd']
# for i in range(len(lineColors)):
# 	plt.plot([1,10],[10+i,10+i],c=lineColors[i])
# plt.show()
lineStyles = ['-', '--', ':', '-.', '-']
lineMarkers = ['o', 's', 's', 'd', 'o', '*', '3']
############# Reading in ##############


def create11fig():
    fig = plt.figure(figsize=(16, 9))
    ax1 = fig.add_subplot(111)

    return ax1


def create31fig():
    fig = plt.figure(figsize=(16, 27))
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)
    plt.subplots_adjust(hspace=0.001)
    plt.subplots_adjust(wspace=0.001)
    ax1.set_xticklabels([])
    ax2.set_xticklabels([])
    xticklabels = ax1.get_xticklabels() + ax2.get_xticklabels()
    plt.setp(xticklabels, visible=False)
    # ax1.set_title(title)
    nbins = len(ax1.get_xticklabels())  # added
    ax2.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))  # added
    # ax2.set_ylabel(ylabel)
    ax3.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))  # added
    # ax3.set_xlabel(xlabel)

    return ax1, ax2, ax3


def create41fig():
    fig = plt.figure(figsize=(16, 36))
    ax1 = fig.add_subplot(411)
    ax2 = fig.add_subplot(412)
    ax3 = fig.add_subplot(413)
    ax4 = fig.add_subplot(414)
    plt.subplots_adjust(hspace=0.001)
    plt.subplots_adjust(wspace=0.001)
    ax1.set_xticklabels([])
    ax2.set_xticklabels([])
    ax3.set_xticklabels([])
    xticklabels = ax1.get_xticklabels() + ax2.get_xticklabels() + ax3.get_xticklabels()
    plt.setp(xticklabels, visible=False)
    # ax1.set_title(title)
    nbins = len(ax1.get_xticklabels())  # added
    ax2.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))  # added
    # ax2.set_ylabel(ylabel)
    ax3.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))  # added
    # ax3.set_xlabel(xlabel)
    ax4.yaxis.set_major_locator(MaxNLocator(nbins=nbins, prune='upper'))  # added

    return ax1, ax2, ax3, ax4

#***********************


def removeLegend(axes):
    for ax in axes:
        ax.legend().set_visible(False)

#***********************


def removeXlabel(axes):
    for ax in axes:
        x_axis = ax.axes.get_xaxis()
        x_label = x_axis.get_label()
        x_label.set_visible(False)

#***********************


def outputFig(fig, saveName=None, saveFig=False):
    if saveFig == True:
        fig.savefig(saveName)

    else:
        plt.show()

#***********************


def meanOfList(alist):

    mean = sum(alist) / float(len(alist))

    return mean
#***********************


def velocityEscape(M0):
    # Escape velocity
    def f(x): return (2. * Grav * M0 / x)**(0.5)

    return f

#***********************


def reactionRate(reaParam):
    # reaction rate UMIST format
    alpha = reaParam["alpha"]
    beta = reaParam["beta"]
    gamma = reaParam["gamma"]

    def f(x): return alpha * (x / 300.)**(beta) * np.exp(-gamma / x)

    return f

#***********************


def powerLaw(x0, y0, p):
    # any power law

    def f(x): return y0 * (x / x0)**(-p)

    return f

#***********************


def densityHydrostaticEquilibrium(beta, mu, version="correct"):
    # Hydrostatic equilibrium, velocity = 0
    T0 = starParam["Tstar"]
    R0 = starParam["Rstar"]
    M0 = starParam["Mstar"]
    rho0 = starParam["rhoStar"]

    if version == "diff":
        def f(r): return ((r / R0)**(beta) - 1.)

    else:
        H0 = kboltz * T0 * R0**2. / (mu * mh * Grav * M0)
        A = R0 / (H0 * (1. - beta))

        def B(r): return (1. - (r / R0)**(beta - 1.))
        if version == "correct":
            def f(r): return rho0 * (r / R0)**(beta) * np.exp(-A * B(r))

        elif version == "cherchneff":
            def f(r): return rho0 * np.exp(-A * B(r))

        else:
            return "version needs to be element of (diff, cherchneff, correct)."

    return f

#***********************


def plotDensityProfiles(rhoFunctionList, rhoVar, fig=99):

    plt.figure(fig)
    cnt = 0
    for func, var in zip(rhoFunctionList, rhoVar):
        style = lineStyles[cnt]
        color = lineColors[1]
        if cnt > 3:
            color = 'k'
        lineStyle = {"style": style, "color": color}
        plotSingleDensityProfile(func, var, lineStyle)
        cnt += 1

    plt.ylabel(r'Density (kg/m$^3$)')
    plt.xlabel('Radius (au)')

    plt.legend(loc='lower left')
    # plt.show()


def plotSingleDensityProfile(rho, var, line):

    r = np.linspace(starParam["Rstar"], starParam["Rend"], 100)
    y = rho(r)
    print var
    if(type(var) == list):
        lab = r"$ \beta = %s,\mu = %s$" % (var[0], var[1])
    else:
        lab = r"power law, $ \alpha = %s$" % (int(var))

    plt.semilogy(r / AU, y, linestyle=line["style"], c=line["color"], label=lab)

#***********************


def plotReactionRate(k, Trange):

    T = np.linspace(Trange[0], Trange[-1], 1000)
    y = k(T)

    plt.loglog(T, y)
    # plt.ylim([1e-21,1e-16])
    #plt.title("Reaction rate")
    plt.xlabel("Temperature (K)")
    plt.ylabel(r"Reaction rate (cm$^3$ s$^{-1}$)")
    # plt.show()

#***********************


def makeDict(keys, values):

    dic = dict()
    for k, v in zip(keys, values):
        dic[k] = v

    return dic


def setNetworkTempDensityParam():
    # temperares and densities used
    # for network analysis
    temperatureList = ["0.1E+04", "0.1E+05", "0.4E+05"]
    densityList = ["0.1E-09", "0.1E-11"]

    return temperatureList, densityList
#***********************


def setStarParam():
    # stellar parameters in SI units
    stringList = ["Rstar", "Rstart", "Rend", "Tstar", "Mstar", "rhoStar"]
    valueList = [1.0 * AU, 1.0 * AU, 10. * AU, 2500., 1.0 * Msol, 1.e-6]

    return stringList, valueList

#***********************


def setReactionParam():
    stringList = ["alpha", "beta", "gamma", "tempRange"]
    valueList = [1.17e-17, -0.14, 0., [10., 1.e4]]

    return stringList, valueList

#***********************


def setDensityParam():
    stringList = ["tempExp", "meanMol", "power"]
    valueList = [[0.4, 0.8], [1.29, 2.35], 10.]
    hydroStat = ["correct"]  # , "cherchneff", "diff"]

    return stringList, valueList, hydroStat

#***********************


def specialSpecies():
    list = ['Hj',
            'HEj',
            'Ek',
            'Cj',
            'Oj',
            'SIj',
            'FEj']

    return list

#***********************


def getSpecies():
    list = ['H',
            'H+',
            'HE',
            'HE+',
            'H2',
            'E-',
            'CO',
            'C',
            'C+',
            'O',
            'O+',
            'SI',
            'SI+',
            'FE',
            'FE+',
            'H2O',
            'CO2',
            'HCN',
            'SIO',
            'SO',
            'SO2',
            'CS',
            'OH',
            'SIO2'
            ]

    return list


def massAtoms():

    me = 9.10938188e-31  # electron mass (kg)
    mp = 1.67262158e-27  # proton mass (kg)
    mn = 1.6725e-27  # neutron mass (kg)
    menp = me + mp + mn
    # mass dictionary
    massDict = {'H': me + mp,
                'D': menp,
                'HE': 2. * (menp),
                'LI': 3. * (me + mp) + 4. * mn,
                'BE': 4. * (me + mp) + 5. * mn,
                'C': 6. * (menp),
                'N': 7. * (menp),
                'O': 8. * (menp),
                'F': 9. * (menp) + mn,
                'NE': 10. * (menp),
                'MG': 12. * (menp),
                'NA': (me + mp) * 11 + mn * 12,
                'AL': (me + mp) * 13 + mn * 14,
                'SI': 14. * (menp),
                'P': 15. * (menp) + mn,
                'S': (menp) * 16,
                'CL': (menp) * 17 + mn,
                'FE': (me + mp) * 26 + mn * 29,
                'E': 0.,
                '-': me,
                '+': -me}

    massDict['CO'] = massDict['O'] + massDict['O']
    massDict['H2'] = 2 * massDict['H']
    massDict['SO'] = massDict['S'] + massDict['O']
    massDict['SO2'] = massDict['S'] + 2*massDict['O']
    massDict['SIO'] = massDict['SI'] + massDict['O']
    massDict['SIO2'] = massDict['SI'] + 2*massDict['O']
    massDict['H2O'] = 2*massDict['H'] + massDict['O']
    massDict['OH'] = massDict['O'] + massDict['H']
    massDict['CO2'] = massDict['C'] + 2*massDict['O']
    massDict['HCN'] = massDict['H'] + massDict['C'] + massDict['N']
    massDict['CS'] = massDict['C'] + massDict['S']

    return massDict


def getMass(elem):
    plus = elem.count('+')
    minus = elem.count('-')
    atom = elem.replace('-', '').replace('+', '')
    masses = massAtoms()
    mass = masses[atom] + plus * masses['+'] + minus * masses['-']

    return mass


def x2n(x, density, spec):
    numberDensity = density * x / getMass(spec)

    return numberDensity
#***********************


def getcoolingLineStyles():
    dict = {'coolAtom': {'style': ':', 'marker': '', 'markerfill': 'none', 'opacity': 0.5},
            'coolCIE': {'style': '-.', 'marker': '', 'markerfill': 'none', 'opacity': 0.25},
            'coolH2': {'style': '--', 'marker': '', 'markerfill': 'none', 'opacity': 0.75},
            'coolZ': {'style': '-', 'marker': '', 'markerfill': 'none', 'opacity': 1},
            'coolCO': {'style': '', 'marker': 'v', 'markerfill': 'full', 'opacity': 1},
            'coolChem': {'style': '', 'marker': 'o', 'markerfill': 'none', 'opacity': 1},
            'heatChem': {'style': '', 'marker': 'o', 'markerfill': 'none', 'opacity': 1},
            'heatCR': {'style': '', 'marker': '+', 'markerfill': 'none', 'opacity': 1},
            }

    return dict

#***********************


def getCoolingLabel():
    dict = {'coolAtom': "H-He cool",
            'coolCO': "CO cool",
            'coolCIE': "CIE cool",
            'coolH2': "H$_2$ line cool",
            'coolZ': "Metal cool",
            'coolChem': "H$_2$ chem cool",
            'heatChem': "H$_2$ chem heat",
            'heatCR': "CR heat"
            }

    return dict

#***********************


def getAbuncanceLineStyles():
    dict = {'SI': {'style': ':', 'marker': '', 'markerfill': 'none', 'opacity': 0.5},
            'CO': {'style': '', 'marker': 'v', 'markerfill': 'full', 'opacity': 1},
            'C': {'style': '-.', 'marker': '', 'markerfill': 'none', 'opacity': 0.25},
            'O': {'style': '--', 'marker': '', 'markerfill': 'none', 'opacity': 0.75},
            'FE': {'style': '-', 'marker': '', 'markerfill': 'none', 'opacity': 1},
            'H2': {'style': '', 'marker': 'o', 'markerfill': 'none', 'opacity': 1},
            'E-': {'style': '', 'marker': '+', 'markerfill': 'none', 'opacity': 1},
            'H': {'style': '', 'marker': 's', 'markerfill': 'none', 'opacity': 1},
            'OH': {'style': ':', 'marker': '', 'markerfill': 'none', 'opacity': 0.5},
            'SO': {'style': '', 'marker': 'v', 'markerfill': 'full', 'opacity': 1},
            'SIO2': {'style': '-.', 'marker': '', 'markerfill': 'none', 'opacity': 0.25},
            'SIO': {'style': '--', 'marker': '', 'markerfill': 'none', 'opacity': 0.75},
            'H2O': {'style': '-', 'marker': '', 'markerfill': 'none', 'opacity': 1},
            'CS': {'style': '', 'marker': 'o', 'markerfill': 'none', 'opacity': 1},
            'SO2': {'style': '', 'marker': '+', 'markerfill': 'none', 'opacity': 1},
            'HCN': {'style': '', 'marker': 's', 'markerfill': 'none', 'opacity': 1},
            'CO2': {'style': '-.', 'marker': '', 'markerfill': 'none', 'opacity': 0.25},
            }

    return dict

#***********************


def getAbundanceLabel():
    dict = {
        'H': 'H',
        'H+': 'H$^+$',
        'HE': 'He',
        'HE+': 'He$^+$',
        'H2': 'H$_2$',
        'E-': 'e$^-$',
        'CO': 'CO',
        'C': 'C',
        'C+': 'C$^+$',
        'O': 'O',
        'O+': 'O$^+$',
        'SI': 'Si',
        'SI+': 'Si$^+$',
        'FE': 'Fe',
        'FE+': 'Fe$^+$',
        'H2O': 'H$_2$O',
        'CO2': 'CO$_2$',
        'HCN': 'HCN',
        'SIO': 'SiO',
        'SO': 'SO',
        'SO2': 'SO$_2$',
        'CS': 'CS',
        'OH': 'OH',
        'SIO2': 'SiO$_2$'
    }

    return dict

#***********************


def doReactionRate():

    paramString, paramValues = setReactionParam()
    reactionParam = makeDict(paramString, paramValues)

    rate = reactionRate(reactionParam)
    temperatureRange = reactionParam["tempRange"]

    plotReactionRate(rate, temperatureRange)

#***********************


def getHydroStat(param, funcList, varList, version):
    # loop over temperature exponents and mean molecular weights
    for temp in param["tempExp"]:
        for mu in param["meanMol"]:
            rhoProfile = densityHydrostaticEquilibrium(temp, mu, version)
            funcList.append(rhoProfile)
            varList.append([temp, mu])


def getPowerLaw(param):
    x0 = starParam["Rstar"]
    y0 = starParam["rhoStar"]
    exp = param['power']

    rhoProfile = powerLaw(x0, y0, exp)
    return rhoProfile, param['power']

    # return [rhoProfile], [param['power']]

#***********************
# def getDensityProfiles(param, version="correct"):
#
# 	fList = []
# 	varList = []
#
# 	# get hydrostatic equilibrium functions
# 	getHydroStat(param, fList, varList, version)
# 	# get power law function
# 	getPowerLaw(param, fList, varList)
#
# 	return fList,varList

#***********************


def doDensity():

    paramString, paramValues, hydroStatVersions = setDensityParam()
    densityParam = makeDict(paramString, paramValues)
    densityProfiles = []
    densityVar = []
    figures = []
    # loop over different versions of hydrostatic equilibrium
    for ver in hydroStatVersions:
        if ver != "diff":
            fig = 0
        else:
            fig = 1
        # get hydrostatic equilibrium functions
        getHydroStat(densityParam, densityProfiles, densityVar, ver)
        # #plot different hydrostatic profiles
        #plotDensityProfiles(densityProfiles, densityVar, fig)

    # get power law function
    profile, variables = getPowerLaw(densityParam)
    #fig = 3
    densityProfiles.append(profile)
    densityVar.append(variables)
    # plot power laws
    #plotDensityProfiles(profile, variables, fig)
    # plot different hydrostatic profiles
    plotDensityProfiles(densityProfiles, densityVar, fig)
#***********************


def showPlots():
    plt.show()

#***********************


def readSnapshot(filename):

    with open(filename, 'r') as f:
        firstLine = f.readline()
        numberOfVariables = int(firstLine.split(" ")[0])
        param = [next(f).strip() for x in xrange(numberOfVariables)]

    param = replaceVariableNames(param)
    rowSkip = numberOfVariables * 2 + 1  # len(param)*2 + 1
    variables = dict()
    for i in range(numberOfVariables):
        variables[param[i]] = np.loadtxt(filename, usecols=(i,), unpack=True, skiprows=rowSkip)

    return variables

#***********************


def replaceVariableNames(list):
    for idx, name in enumerate(list):
        name = name.replace("1D_Radius_AU", "radius")
        if name in specialSpecies():
            name = name.replace("k", "-")
            name = name.replace("j", "+")
        list[idx] = name

    return list

#***********************


def doStructure(files, model=None, singlePlot=False, saveFig=False, figName=None):

    if saveFig == True and figName == None:
        print "Provide a figure name when saving a figure"
        return

    dictOfSnapshots = dict()
    for file in files:
        if model == 'escape':
            # key is velocity amplitude e.g. v20k
            key = re.search(r'_v(...)', file).group(1)
        else:
            # key is number of days. e.g. 1200days
            key = re.search(r'([0-9]+days)', file).group(1)
        # dictionairy of dictionairies
        dictOfSnapshots[key] = readSnapshot(file)

    if not model:
        # made for three variables (sub or single plots)
        if singlePlot == False:
            ax1, ax2, ax3 = create31fig()
        else:
            ax1 = create11fig()
            ax2 = create11fig()
            ax3 = create11fig()

        allAxes = [ax1, ax2, ax3]

        plotDensityStructureEvolution(dictOfSnapshots, ax1)
        plotVelocityStructureEvolution(dictOfSnapshots, ax2)
        plotTemperatureStructureEvolution(dictOfSnapshots, ax3)
        # plotAbundaceStructure(subDictSpecies, ax1)
        # plotTemperatureStructureEvolution(subDictTemperature, ax2)
        # plotDensityStructureEvolution(subDictTemperature, ax3)

        if singlePlot == False:
            removeLegend([ax2, ax3])
            removeXlabel([ax1, ax2])

            fig = ax1.get_figure()
            outputFig(fig, figName, saveFig=saveFig)

        else:
            for idx, ax in enumerate(allAxes):
                fig = ax.get_figure()
                if figName:
                    newfigName = figName + str(idx)
                outputFig(fig, newfigName, saveFig=saveFig)

    elif model == 'escape':
        fig = plt.figure('velocity')
        ax1 = fig.add_subplot(111)
        radius = np.linspace(starParam["Rstar"], starParam["Rend"], 100)
        velEsc = velocityEscape(starParam["Mstar"])
        velocity = velEsc(radius)
        style = '-'
        color = 'k'
        opacity = 1
        label = "$v_{\\text{esc}}$"
        lineStyle = {"style": style, "color": color, "opacity": opacity, "label": label}
        plotVelocityStructureSnapshot(ax1, radius / AU, velocity, lineStyle)

        plotVelocityStructureEvolution(dictOfSnapshots, ax1, model)

    elif model == 'cooling':
        # make separate figure for each time snapshot
        for key in sorted(dictOfSnapshots.iterkeys()):
            # ax1, ax2, ax3, ax4 = create41fig()
            ax1, ax2, ax3 = create31fig()

            dictOfVariables = dictOfSnapshots[key]
            # make sub dict to be compatible with plotTemperatureStructureEvolution
            subDictTemperature = dict()
            subDictTemperature[key] = dictOfVariables
            # make sub dict with only heating and cooling terms
            # subDictCooling = dict((k, dictOfVariables[k])
            #                       for k in dictOfVariables if ('cool' in k or 'heat' in k))
            # subDictCooling['radius'] = dictOfVariables['radius']
            # make sub dict with only species
            subDictSpecies = dict((k, dictOfVariables[k])
                                  for k in dictOfVariables if k in getSpecies())
            subDictSpecies['radius'] = dictOfVariables['radius']
            subDictSpecies['rho'] = dictOfVariables['rho']
            # fig = plt.figure()
            # ax1 = fig.add_subplot(111)
            # plotCoolingStructure(subDictCooling, ax1)
            # plotAbundaceStructure(subDictSpecies, ax2)
            # plotTemperatureStructureEvolution(subDictTemperature, ax3)
            # plotDensityStructureEvolution(subDictTemperature, ax4)
            plotAbundaceStructure(subDictSpecies, ax1)
            plotTemperatureStructureEvolution(subDictTemperature, ax2)
            plotDensityStructureEvolution(subDictTemperature, ax3)

            # removeLegend([ax3, ax4])
            # removeXlabel([ax1, ax2, ax3])
            removeLegend([ax3])
            removeXlabel([ax1, ax2])
            # plt.show()
            plt.savefig("Revision_structure_v20k_extra_mol_" + str(key))

#***********************


def plotTemperatureStructureSnapshot(fig, x, y, line):

    fig.semilogy(x, y, ls=line["style"], c=line["color"], \
                 # fig.plot(x, y, ls=line["style"], c=line["color"], \
                 alpha=line["opacity"], label=line["label"])
    fig.set_ylabel('Gas temperature (K)')
    fig.set_xlabel('Radius (au)')
    # fig.legend()

    if min(y) > 1000:
        fig.set_ylim(bottom=900, auto=True)

    if (max(y) > 8000 and max(y) < 10100):
        fig.set_ylim(top=12000, auto=True)

#***********************


def plotTemperatureStructureEvolution(snapshots, fig):

    cnt = 0
    increment = 1. / (len(snapshots))
    for key in sorted(snapshots.iterkeys()):
        variables = snapshots[key]
        temp = variables["Tgas"]
        radius = variables["radius"]

        style = lineStyles[cnt]
        color = lineColors[1]
        opacity = 1 - cnt * increment
        label = str(int(key[:len(key) - 4])) + " d"
        lineStyle = {"style": style, "color": color, "opacity": opacity, "label": label}

        plotTemperatureStructureSnapshot(fig, radius, temp, lineStyle)
        cnt += 1

    # plt.show()

#***********************


def plotVelocityStructureSnapshot(fig, x, y, line):

    fig.plot(x, y / 1000., ls=line["style"],
             c=line["color"], alpha=line["opacity"], label=line["label"])

    fig.set_ylabel('Velocity (km/s)')
    fig.set_xlabel('Radius (au)')
    fig.legend()

#***********************


def plotVelocityStructureEvolution(snapshots, fig, escape=None):

    cnt = 0
    if not escape:
        increment = 1. / (len(snapshots))

    for key in sorted(snapshots.iterkeys()):
        variables = snapshots[key]
        velocity = variables["v1"]
        radius = variables["radius"]

        if not escape:
            style = lineStyles[cnt]
            color = lineColors[1]
            opacity = 1 - cnt * increment
            label = str(int(key[:len(key) - 4])) + " d"
            lineStyle = {"style": style, "color": color, "opacity": opacity, "label": label}

        else:
            style = lineStyles[cnt]
            color = lineColors[cnt]
            keyRegex = re.search(r'([0-9]+)k([0-9]?)', key)
            label = int(keyRegex.group(1))
            if keyRegex.group(2):
                label += int(keyRegex.group(2)) * 1e-1
            label = r"$\Delta v =$ %s km/s" % (str(label))
            lineStyle = {"style": style, "color": color, "opacity": 1, "label": label}

        plotVelocityStructureSnapshot(fig, radius, velocity, lineStyle)
        cnt += 1

#***********************


def plotDensityStructureSnapshot(fig, x, y, line):

    fig.semilogy(x, y, ls=line["style"], c=line["color"],
                 alpha=line["opacity"], label=line["label"])
    fig.set_ylabel(r'Density (kg/m$^3$)')
    fig.set_xlabel('Radius (au)')
    fig.legend()

#***********************


def plotDensityStructureEvolution(snapshots, fig):

    cnt = 0
    increment = 1. / (len(snapshots))
    for key in sorted(snapshots.iterkeys()):
        variables = snapshots[key]
        rho = variables["rho"]
        radius = variables["radius"]

        style = lineStyles[cnt]
        color = lineColors[1]
        opacity = 1 - cnt * increment
        label = str(int(key[:len(key) - 4])) + " d"
        lineStyle = {"style": style, "color": color, "opacity": opacity, "label": label}
        plotDensityStructureSnapshot(fig, radius, rho, lineStyle)
        cnt += 1

#***********************


def plotCoolingStructure(processes, fig):

    cnt = 0
    styleDict = getcoolingLineStyles()
    labelDict = getCoolingLabel()
    for key in sorted(processes.iterkeys()):
        if key == 'radius':
            continue
        if key == 'rho':
            continue
        values = processes[key] * 0.1  # cgs to SI
        radius = processes['radius']

        if 'heat' in key:
            color = 'r'
        else:
            color = lineColors[1]

        lineStyle = {"color": color, "label": labelDict[key]}
        lineStyle.update(styleDict[key])

        if key == 'coolChem' or key == 'heatChem':
            indices = [idx for idx, item in enumerate(values) if item < 0]

            radius = np.delete(radius, indices)
            values = np.delete(values, indices)

        # replace zero/really small values with small value
        values[values < 1e-32] = 1e-32

        # execute string as command for flexible legend handling
        singleplot = key + ',=plotCoolingSnapshot(fig, radius, values, lineStyle)'
        exec(singleplot)

    fig.legend(handles=[coolZ, coolH2, coolAtom, coolCIE, coolCO,
                        heatCR, coolChem, heatChem], loc='upper center', ncol=4)

    return
#***********************


def plotCoolingSnapshot(fig, x, y, line):

    line, = fig.semilogy(x, y, linestyle=line["style"], marker=line["marker"],
                         fillstyle=line["markerfill"], markevery=0.01,
                         c=line["color"], label=line["label"], alpha=line["opacity"])
    # fig.set_yscale('symlog')
    fig.set_ylim(bottom=1e-16, top=1e-6, auto=True)
    fig.set_ylabel(r'Cooling rate (J s$^{-1}$ m$^{-3}$)')
    fig.set_xlabel('Radius (au)')
    fig.legend(loc='upper center', ncol=4)
    # plt.show()
    return line,
#***********************
#***********************


def plotAbundaceStructure(processes, fig):

    cnt = 0
    styleDict = getAbuncanceLineStyles()
    labelDict = getAbundanceLabel()
    for key in sorted(processes.iterkeys()):
        if key not in styleDict:
            continue
        if key == 'radius':
            continue
        massfrac = processes[key]
        radius = processes['radius']
        density = processes['rho']
        #values = massfrac
        values = x2n(massfrac, density, key)  # number per m^3
        color = lineColors[1]

        lineStyle = {"color": color, "label": labelDict[key]}
        lineStyle.update(styleDict[key])

        singleplot = key.replace("-", "k").replace("+", "j") + \
            ', =plotAbundanceSnapshot(fig, radius, values, lineStyle)'
        exec(singleplot)

    # fig.legend(handles=[FE, O, C, SI, H, H2, CO, Ek], loc='upper center', ncol=4)
    fig.legend(handles=[H2O, SIO, SIO2, OH, HCN, CS, SO, SO2], loc='upper center', ncol=4)

    return
#***********************


def plotAbundanceSnapshot(fig, x, y, line):

    line, = fig.semilogy(x, y, linestyle=line["style"], marker=line["marker"],
                         fillstyle=line["markerfill"], markevery=0.01,
                         c=line["color"], label=line["label"], alpha=line["opacity"])
    # fig.set_yscale('symlog')
    fig.set_ylim(bottom=1e4, top=1e17, auto=True)
    fig.set_ylabel(r'Number density (m$^{-3}$)')
    fig.set_xlabel('Radius (au)')
    fig.legend(loc='upper center', ncol=4)
    # plt.show()
    return line,

#***********************


def doNetworkAnalysis(path):

    filenames = []
    react0030 = dict()
    react0300 = dict()
    react0700 = dict()
    react1800 = dict()
    dictnames = [react0030, react0300, react0700, react1800]

    for root, dirs, files in os.walk(path):
        place = []
        for file in files:
            if file.endswith(".dat"):
                # if file.startswith("T_0.4E+05_rho_0.1E-09"):
                place.extend(glob.glob(os.path.join(root,  file)))
        if place:
            place.sort()
            filenames.append(place)
    filenames.sort()
    react0030[".dat"] = filenames[0]
    react0300[".dat"] = filenames[1]
    react0700[".dat"] = filenames[2]
    react1800[".dat"] = filenames[3]

    dictnames = getNetworkData(dictnames)
    temperatureDensityGrid = getTemperatureDensityGrid(3, 2)
    plotCPUtimePerNetwork(dictnames, temperatureDensityGrid)

#***********************


def getNetworkData(dictnames):

    # find cpu time data
    temperatures, densities = setNetworkTempDensityParam()
    for dic in dictnames:
        for temp in temperatures:
            for rho in densities:
                data = []
                # find correct files for correct lists
                for name in dic[".dat"]:
                    name_file = name.split('/')[-1]
                    ending = name_file.split('_')[-1]
                    temprho = "T_" + temp + "_rho_" + rho
                    if temprho in name_file:
                        if (name_file.startswith("Cooling") or name_file.startswith("Heating")):
                            continue
                        else:
                            if "_.dat" in name_file:
                                data.append(name)

                dic[temprho] = data

    return dictnames

#***********************


def getTemperatureDensityGrid(Ntemp, Nrho):
    # Choose which temperature-density combos you want for plotting
    # NOT YET for random combo's, only for ascending in lists
    grid = []
    temperatures, densities = setNetworkTempDensityParam()
    for idxTem in range(Ntemp):
        for idxDen in range(Nrho):
            temp = temperatures[idxTem]
            density = densities[idxDen]
            grid.append("T_" + temp + "_rho_" + density)
    return grid

#***********************


def plotCPUtimePerNetwork(networks, temperatureDensityGrid):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    # different network sizes
    x = np.array([29, 323, 719, 1872])

    cpuAllNetworks = []
    cntNtw = 0
    for ntw in networks:
        cpuNetwork = []
        for td in temperatureDensityGrid:
            for file in ntw[td]:
                cputime = np.loadtxt(file, unpack=True, skiprows=1, usecols=(0))
                totalCputime = cputime.sum()
                meanCputime = meanOfList(cputime)
                # cpuNetwork.append(totalCputime)
                cpuNetwork.append(meanCputime)
                # sorry for the hardcoded value 2.86830601093e-05 = cpuAllNetworks[0]
                scat = ax.scatter(x[cntNtw], meanCputime / 2.86830601093e-05,
                                  color='k', zorder=99, label='Individual tests')

        cntNtw += 1
        meanCpuNetwork = meanOfList(cpuNetwork)
        cpuAllNetworks.append(meanCpuNetwork)

    y = cpuAllNetworks / cpuAllNetworks[0]

    line, = ax.plot(x, y, '-', markersize=20, label='Mean')
    ax.set_xlim(left=0, right=2000, auto=True)
    ax.set_ylim(top=530, auto=True)
    ax.set_ylabel('Increase in computation time  \n (factor w.r.t. size of 30)')
    ax.set_xlabel('Network size (number of reactions)')
    ax.legend(handles=[scat, line])


#***********************
def main():
    global starParam

    option = sys.argv[1]

    paramString, paramValues = setStarParam()
    starParam = makeDict(paramString, paramValues)

    if option == 'reaction':

        doReactionRate()

    elif option == 'density':

        doDensity()
        plt.savefig("initDensity")

    elif option == 'struct':

        if len(sys.argv) > 2:
            structOption = sys.argv[2]
        else:
            structOption = None

        if not structOption:
            structureFiles = [
                "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_nochem_paper_t000days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_cool_paper_t020days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_cool_paper_t100days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_cool_paper_t300days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_cool_paper_t600days.okc"
            ]
            figName = 'Revision_structure_v2k5_cool'
            # structureFiles = [
            # "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_nochem_paper_t000days.okc",
            # "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_nochem_paper_t100days.okc",
            # "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_nochem_paper_t200days.okc",
			# "/media/jels/Elements/IvS/Results/Paper/UCL_v2k5_nochem_paper_t300days.okc"
            # ]
        elif structOption == 'escape':

            structureFiles = [
                "/media/jels/Elements/IvS/Results/Paper/nochem_v2k5_t2240days.okc",
                "/media/jels/Elements/IvS/Results/Paper/nochem_v20k_t2240days.okc"]

        elif structOption == 'cooling':

            structureFiles = [
                "/media/jels/Elements/IvS/Results/Paper/UCL_v20k_cool_paper_extra_mol_t020days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v20k_cool_paper_extra_mol_t100days.okc",
                "/media/jels/Elements/IvS/Results/Paper/UCL_v20k_cool_paper_extra_mol_t300days.okc"
            ]

        else:
            print 'Argument is not valid'

        # , figName=figName, saveFig=False)
        doStructure(structureFiles, structOption, singlePlot=False, saveFig=False)#, figName=figName)
        # plt.savefig('escape')

    elif option == 'network':

        path = "/media/jels/Elements/IvS/KROME/output/Impact_size_and_cooling/"
        doNetworkAnalysis(path)
        plt.savefig('cputime')
        # plt.show()

    else:
        print 'Argument is not valid'


if __name__ == "__main__":
    main()
