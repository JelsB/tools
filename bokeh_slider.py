from bokeh.io import show,curdoc
from bokeh.layouts import column, row, gridplot
from bokeh.models import Slider, CustomJS, ColumnDataSource, Range1d, Plot
from bokeh.plotting import Figure, figure
from bokeh.models.glyphs import ImageURL
from bokeh.resources import CDN
from bokeh.embed import autoload_static, components
import sys
from argparse import Namespace


# js_file = 'test.js'
# rel_js_path = '/assets/bokeh/js/'
# rel_fig_path = '/assets/bokeh/figs/'
# abs_js_path = '/home/jels/codes/dev_bitbucket_pages/'+rel_js_path+js_file
APP_PATH ='/home/jels/codes/dev_bitbucket_pages'
path = ('/home/jels/codes/krome/tools/kexplorer/pngs_paper2/'
       'all_gen_nucleation_big_ntw_xtr_Mg_rev_no_ions_tab_refine/timeloop/')
# path = rel_fig_path
species = []
# species += ["TIO2",
#             "TI2O4","TI3O6",
#             "TI4O8"]
# species += ["TI5O10","TI6O12",
#             "TI7O14","TI8O16","TI9O18",
#             "TI10O20"]
# species += ["SIO", "SI2O2", "SI3O3", "SI4O4", "SI5O5"]
# species += ["SI6O6", "SI7O7", "SI8O8", "SI9O9"]
#
# species += ["MGO", "MG2O2","MG3O3","MG4O4", "MG5O5"]
# species += ["MG6O6", "MG7O7","MG8O8", "MG9O9", "MG10O10"]

species += ["AL2O3", "AL4O6", "AL6O9", "AL8O12", "AL10O15"]
species += ["AL12O18", "AL14O21"]

case_options = Namespace(test_elem=['AL4O6', 'AL6O9', 'AL2O3'],
                         all_elem=species)

script_options = Namespace(save=False,
                           app=False
                           )

app_options = Namespace(js_path='/assets/academia/bokeh/js/monomer/SiO/',
                        tag_js_path=r'{{ page.js_dir }}/',
                        fig_path='/assets/academia/bokeh/figs/monomer/SiO/')


local_options = Namespace(fig_path='/home/jels/codes/krome/tools/kexplorer/'
                                    'pngs_paper2/Al2O3_nucleation/timeloop/')

file_options = Namespace(init_num='000',
                         ext='png')




def set_figure_path():
    if script_options.app:
        file_options.path = app_options.fig_path
    else:
        file_options.path = local_options.fig_path


def figure_slider_js_callback(source):
    """
    TODO still needs flexibility with input param
    """

    return CustomJS(args=dict(source=source), code="""
        var data = source.data;
        var f = cb_obj.value/5
        var file_name = data['url_full']
        var file_num = data['url_num']
        var f_string = f.toString().padStart(3, '0')
        file_name[0] = file_name[0].replace(file_num[0], f_string)
        file_num[0] = f_string
        source.change.emit();
        """)



def multiple_figure_slider_js_callback(sources):
    """
    TODO still needs flexibility with input param

    DOES NOT WORK.....
    """
    js_list = ['data', 'file_name', 'file_num']
    source_keys = []
    js_keys = dict()
    source_dict = dict()

    for i in range(len(sources)):
        key = f'source{i}'
        source_keys.append(key)
        js_keys[key] = {f'{item}{i}' for item in js_list}
        source_dict[key] = sources[i]


    code_vars = ('var f = cb_obj.value/5\n'
                'var f_string = f.toString().padStart(3, "0")\n'
                )
    code_replace = ''
    code_change = ''

    for source in source_keys:
        data, file_name, file_num = js_keys[source]

        code_vars += (f'var {data} = {source}.data;\n'
                     f'var {file_name} = {data}["url_full"]\n'
                     f'var {file_num} = {data}["url_num"]\n'
                     )

        code_replace += (f'{file_name}[0] = {file_name}[0].replace'
                         f'({file_num}[0], f_string)\n'
                         f'{file_num}[0] = f_string\n'
                         )

        code_change += f'{source}.change.emit();\n'

    code_block = f'{code_vars}{code_replace}{code_change}'
    # print(code_block)
    # sys.exit()

    return CustomJS(args=source_dict, code=code_block)

    # return CustomJS(args=dict(source1=source1, source2=source2), code="""
    #
    #
    #     var data1 = source1.data;
    #     var file_name1 = data1['url_full']
    #     var file_num1 = data1['url_num']
    #
    #     var data2 = source2.data;
    #     var file_name2 = data2['url_full']
    #     var file_num2 = data2['url_num']
    #
    #     file_name1[0] = file_name1[0].replace(file_num1[0], f_string)
    #     file_num1[0] = f_string
    #
    #
    #     file_name2[0] = file_name2[0].replace(file_num2[0], f_string)
    #     file_num2[0] = f_string
    #
    #     source1.change.emit();
    #     source2.change.emit();
    #
    #     """)

    # return CustomJS(args=dict(sources=sources), code="""
    #     var all_sources = sources.its;
    #     for (var source in all_sources) {
    #         var data = source.data;
    #
    #         var file_name = data['url_full']
    #         var file_num = data['url_num']
    #         var f = cb_obj.value/5
    #         var f_string = f.toString().padStart(3, '0')
    #
    #
    #         file_name[0] = file_name[0].replace(file_num[0], f_string)
    #         file_num[0] = f_string
    #         source.change.emit();
    #     };
    #     """)



def show_output(layout):
    show(layout)


def save_js_script_and_tag(layout, script_input_path, script_path):
    """
    TODO: find solution to save tag.
    """
    js_script, tag = autoload_static(layout, CDN, script_input_path)
    with open(script_path, 'w') as ff:
        ff.write(js_script)
        print(tag)
        # print(f'used next script tag for the JS script:\n {tag}')



def make_column_image_slider(data, num_plots=1, num_sliders=1,
                             global_slider=False):
    plots = []
    sliders = []
    sources = []#dict()

    # sys.exit()
    for i in range(num_plots):
        local_data = dict()

        for key in data.keys():
            local_data[key] = [data[key][i]]
        local_source = ColumnDataSource(data=local_data)
        sources.append(local_source)
        # sources['its'] = local_source
        plots.append(make_image_slider_plot(local_source))
    # print(sources)

    # for _ in range(num_sliders):
    # def py_callback(source=local_source, window=None):
    #     data = source.data
    #     ff = cb_obj.values/5
    #     file_name = data['url_full']
    #     file_num = data['url_num']
    #     f_string = f'{f:03d}'
    #     file_name[0] = file_name[0].replace(file_num[0], f_string)
    #     file_num[0] = f_string
    #     source.change.emit()

    # sliders.append(
    #         make_single_slider(start=0, end=73*5, value=0, step=5,
    #                             title="days",
    #                             callback=figure_slider_js_callback(local_source))
    #                             # callback=CustomJS.from_py_func(py_callback))
    #             )
    if global_slider:
        sliders.append(
                make_single_slider(start=0, end=73*5, value=0, step=5,
                                    title="days",
                                    callback=multiple_figure_slider_js_callback(sources))
                )
    column_parts = sliders + plots

    return column(column_parts)

def make_grid_image_slider(data, num_plots=1, num_sliders=1,
                             global_slider=False):
    plots = []
    sliders = []
    sources = []
    num_colums = 3

    num_rows = num_plots//num_colums
    if num_plots%num_colums != 0:
         num_rows += 1
    #extra row for slider
    num_rows += 1

    for i in range(num_plots):
        local_data = dict()

        for key in data.keys():
            local_data[key] = [data[key][i]]

        local_source = ColumnDataSource(data=local_data)
        sources.append(local_source)
        plots.append(make_image_slider_plot(local_source))


    if global_slider:
        sliders.append(
                make_single_slider(start=0, end=73*5, value=0, step=5,
                                    title="days",
                                    callback=multiple_figure_slider_js_callback(sources))
                )

    # sliders.extend([None]*(num_colums-1))
    plot_parts = chuck_list(plots, num_colums)
    # if len(plot_parts[-1]) < num_colums:
        # plot_parts[-1].extend([None]*(num_colums-len(plot_parts[-1])))

    grid_parts = [sliders] + plot_parts
    return gridplot(grid_parts)


def chuck_list(lst, chucks_size):
    return [lst[i:i+chucks_size] for i in range(0, len(lst), chucks_size)]


def make_single_slider(start=0, end=10, value=0, step=1, title='Title',
                       callback=None):

    return Slider(start=start, end=end, value=value, step=step, title=title,
                  callback=callback)


def make_image_slider_plot(source):

    tools = ''
    plot = figure(plot_width=700, plot_height=500, x_range=(0,1), y_range=(0,1),
               tools=tools)
    plot.image_url('url_full',source=source, x=0, y=1, w=1, h=1)
    plot.axis.visible = False
    plot.grid.visible = False

    # slider = Slider(start=1, end=73, value=1, step=1, title="image number",
    #                 callback=CustomJS.from_py_func(py_callback))

    return plot#, sizing_mode = 'scale_both')


def make_image_url_dict(main_names):
    full_file_paths = []
    for name in main_names:
        file_name = make_image_file_name(name, file_options.init_num,
                                         file_options.ext)
        full_file_paths.append(file_options.path + file_name)

    init_nums = [file_options.init_num] * len(main_names)

    return dict(url_full=full_file_paths, url_num=init_nums)



def make_image_file_name(front_part, init_file_num, ext):
    return f'{front_part}_{init_file_num}.{ext}'


def make_image_slider(main_name, save=False, **kwargs):

    image_url_dict = make_image_url_dict(main_name)
    # fig = make_column_image_slider(image_url_dict, **kwargs)
    fig = make_grid_image_slider(image_url_dict, **kwargs)

    return fig


def make_multiple_image_sliders_and_save(loop_list):
    for item in loop_list:
        fig = make_image_slider(item)

        # if len(full_file_path) > 100:
        #     print('File path is probably incorrect for saving.\n'
        #           'Change \"file_options.path\" variable.\n'
        #           f'Current file path is {full_file_path}')
        #     sys.exit()

        script_path, script_src_tag_path = make_js_paths(item)

        save_js_script_and_tag(fig, script_src_tag_path, script_path)


def make_js_paths(name):
    script_path = f'{APP_PATH}{app_options.js_path}{name}.js'
    script_src_tag_path = f'{app_options.tag_js_path}{name}.js'

    return script_path, script_src_tag_path


def make_multiple_image_global_slider_and_save(looplist):
    fig = make_image_slider(looplist, num_sliders=1,
                            num_plots=len(looplist), global_slider=True)

    js_name = f'{looplist[0]}_all'
    script_path, script_src_tag_path = make_js_paths(js_name)

    save_js_script_and_tag(fig, script_src_tag_path, script_path)

if __name__=='__main__':
    set_figure_path()

    if script_options.save:
        # make_multiple_image_sliders_and_save(case_options.all_elem)
        make_multiple_image_global_slider_and_save(case_options.all_elem)

    else:
        input = case_options.all_elem
        fig = make_image_slider(input, num_sliders=1,
                                num_plots=len(input), global_slider=True)
        show_output(fig)



# show_output(layout)
# save_js_script_and_tag(layout, script_input_path=rel_fig_path,
#                        script_path=rel_js_path+js_file)
