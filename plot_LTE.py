import sys,os
import glob, os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import re
#plt.style.use('presentation')

font = {'size'   : 21}
lines = {'linewidth' : 5, 'markersize': 10, 'markeredgewidth': 3, }
savefig = {'dpi': 200, 'format': 'png', 'transparent': True,'bbox': 'tight'}
#figure = {'figsize': (32, 18)}

plt.rc('font', **font)
plt.rc('lines', **lines)
plt.rc('savefig', **savefig)
#plt.rc('figure', **figure)
plt.rc('xtick.major', size=10, width=1.5)
plt.rc('xtick.minor', size=5, width=1.5)
plt.rc('ytick.major', size=10, width=1.5)
plt.rc('ytick.minor', size=5, width=1.5)


filenames=[]
react0030 = dict()
react0300 = dict()
react0700 = dict()
react1800 = dict()
dictnames = [react0030,react0300,react0700,react1800]

for root, dirs, files in os.walk("./output"):
	place = []
	for file in files:
		if file.endswith(".dat"):
 		#if file.startswith("T_0.4E+05_rho_0.1E-09"):
			place.extend(glob.glob(os.path.join(root,  file)))
	if place:
		place.sort()
 		filenames.append(place)
filenames.sort()
react0030[".dat"] = filenames[0]
react0300[".dat"] = filenames[1]
react0700[".dat"] = filenames[2]
react1800[".dat"] = filenames[3]


temp_list = ["0.1E+04","0.1E+05","0.4E+05"]
rho_list  = ["0.1E-09","0.1E-11"]
cool_list = ["Tgas","totcool","H2","H2GP","CEN","HD","Z","ENTHALPIC","DUST","COMPTON"\
			,"CIE","CONTINUUM","EXP","BSS","CUSTOM","CO","ZCIE","ZCIENOUV","ZEXTEND"]
heat_list = ["Tgas","totheat","CHEM","COMPR","PHOTO","ENTHALPIC","PHOTOAV","CR","DUST","XRAY","VISC","CUSTOM","ZCIE"]
heatcool_list = [heat_list,cool_list]


cool_relevant = ["totcool","H2","CEN","Z","COMPTON","CONTINUUM","CO"] # choose cooling
heat_relevant = ["totheat","CHEM"]
heatcool_relevant = [heat_relevant,cool_relevant]

for dicts in dictnames:
	# dictionairies per T-rho combo
	dicts["heatCool"] = dict()
	dicts["noheatCool"] = dict()
	dicts["heatCool_eff"] = dict()
	dicts["gammas"] = dict()
	dicts["gammas_heatCool_eff"] = dict()

	for temp in temp_list:
		for rho in rho_list:
			list_heatcool = []				#list with all heat/cool .dat files
			list_noheatcool = []			#list with all NO heat/cool .dat files
			list_heatcool_eff =[]			#list with all heat/cool efficiency files for Gamma =5/3
			list_gammas = []				#list with all Gamma .dat files
			list_gammas_heatcool_eff = []	#list with all heat/cool efficiency files all

			# find correct files for correct lists
			for name in dicts[".dat"]:
				name_file = name.split('/')[-1]
				ending = name_file.split('_')[-1]
				temprho = "T_"+ temp +"_rho_"+rho
				if temprho in name_file:
					if (name_file.startswith("Cooling") or name_file.startswith("Heating")):
						if "COMPTON.dat" in ending:
							list_heatcool_eff.append(name)
						elif "Gamma" in ending:
							list_gammas_heatcool_eff.append(name)
					else:
						if "Gamma" in ending:
							list_gammas.append(name)
						else:
							list_heatcool.append(name)
						if "COMPTON.dat" in ending:
							list_gammas.append(name)
						if "_.dat" in name_file:
							list_noheatcool.append(name)

			dicts["heatCool"][temprho] = list_heatcool
			dicts["noheatCool"][temprho] = list_noheatcool
			dicts["gammas"][temprho] = list_gammas
			dicts["heatCool_eff"][temprho] = list_heatcool_eff[::-1] #first heating then cooling
			dicts["gammas_heatCool_eff"][temprho] = list_gammas_heatcool_eff

#print react1800["heatCool_eff"]
# print dictnames[-1]["heatCool_eff"]['T_0.1E+05_rho_0.1E-11']
#print dictnames[-1]["heatCool"]['T_0.1E+05_rho_0.1E-11']


secondsPerYear=365.*24.*3600.
colors = ['r','b','k','g','m','c','grey','brown','pink']
styles = [':','-','--','-.']				# network size combo's
markers = ['D','+',',','o','s','*']
label_reats = ['30','300','700','1800']

# Plots CPUtime i.f.o. time for a certain "heatCool_key" (e.g. "noheatCool")
# for multiple temperature-density combos listed in "Trho_keys" (e.g. ["T_0.1E+05_rho_0.1E-11",...] )
# for all different networks stated in the list "network_dicts"
def plot_CPUtime_per_network(network_dicts,heatCool_key,Trho_keys,norm=False):

	if len(Trho_keys) > 1 and heatCool_key != 'noheatCool':
		print "ERROR: Not a valid combination. Don't use more than 2 variables"
		return False

	# create handles needed for legends
	color_handles = []
	color_labels  = []
	style_handles = []
	style_labels  = []

	# counter for the network
	cnt_ntw = 0

	# loop over all networks
	# type = dictionary
	for ntw in network_dicts:
		# counter for the T-rho combo
		cnt_Trho = 0

		# loop over all T-rho combo's
		# equals one for "heatCool" case
		for Trho_key in Trho_keys:
			# counter for the different heatCool mechanism files
			cnt_heatCool = 0

			# labels for title
			T_label = Trho_key.split('_')[1]
			T_label = int(float(T_label))
			rho_label1 = Trho_key.split('_')[-1].split('E')[0]
			rho_label2 = Trho_key.split('_')[-1].split('E')[1]
			rho_label1 = int((float(rho_label1)*10))
			rho_label2 = int(rho_label2)-1

			# loop over all files
			# equals one for "noheatCool" case
			for file in ntw[heatCool_key][Trho_key]:
				cputime, time = np.loadtxt(file, unpack=True, skiprows=1, usecols=(0,1))
				style_label	= str(int(re.split('(\d+)',file.split('/')[-2])[1]))+ ' reactions'
				style_index = cnt_ntw

				if heatCool_key =='heatCool':
					color_index = cnt_heatCool
					color_label = file.split('/')[-1].split('.dat')[0].split("_")[-1] + " cooling"

					if color_label == " cooling":
						color_label = "No cooling"

				elif heatCool_key == 'noheatCool':
					color_index = cnt_Trho
					color_label = r'%i K $%i\cdot 10^{%i}$ g/cm$^3$' %(T_label,rho_label1,rho_label2)

				elif heatCool_key == 'gammas':
					color_index = cnt_heatCool
					color_label = file.split('/')[-1].split('.dat')[0].split("_")[-1]

					if color_label.startswith('all'):
						color_label = 'Gamma5/3'

				if norm == True:
					if cnt_heatCool == 0:
						cputime_norm = cputime
					cputime = cputime/cputime_norm
					line, = plt.plot(time/3600.,cputime, ls=styles[style_index], c=colors[color_index] ,lw=5)
				else:
					line, = plt.semilogy(time/3600.,cputime, ls=styles[style_index], c=colors[color_index] ,lw=5)

				if styles[style_index] == "-" or len(network_dicts)==1:
					color_handles.append(line)
					color_labels.append(color_label)
				if colors[color_index] == colors[0]:
					style_handles.append(line)
					style_labels.append(style_label)

				cnt_heatCool = cnt_heatCool + 1
			cnt_Trho = cnt_Trho + 1
		cnt_ntw = cnt_ntw + 1

	plt.xlabel('time (hrs)')

	if norm == True:
		plt.ylabel('fractional cputime w.r.t. normalisation')
	else:
		plt.ylabel('cputime (s)')

	if heatCool_key == 'noheatCool':
		plt.title('CPU time per network for temperature-density grid')
		legend1 = plt.legend(color_handles,color_labels,loc=1)
		ax = plt.gca().add_artist(legend1)
		legend2 = plt.legend(style_handles,style_labels,loc=9, bbox_to_anchor=(0.72, 1))

	elif heatCool_key == 'heatCool':
		print Trho_keys
		plt.title(r'CPU time per network per heating/cooling for initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm$^3$' \
		%(T_label,rho_label1,rho_label2))
		legend1 = plt.legend(color_handles,color_labels,loc=1)
		ax = plt.gca().add_artist(legend1)
		legend2 = plt.legend(style_handles,style_labels,loc=9, bbox_to_anchor=(0.65, 1))

	else:
		plt.title(r'CPU time per network per gamma calculation for initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm$^3$' \
		%(T_label,rho_label1,rho_label2))
		legend1 = plt.legend(color_handles,color_labels,loc=1)
		ax = plt.gca().add_artist(legend1)
		legend2 = plt.legend(style_handles,style_labels,loc=9, bbox_to_anchor=(0.78, 1))


def plot_heatCool_efficiencies(network_dicts,heatCool_key,Trho_keys,mechanisms,subplot=False):

	# create handles needed for legends
	color_handles = []
	color_labels  = []
	style_handles = []
	style_labels  = []

	# counter for the network
	cnt_ntw = 0

	# loop over all networks
	# type = dictionary
	for ntw in network_dicts:
		# counter for T-rho combo's
		cnt_Trho = 0

		# loop over all T-rho combo's
		# equals one for "heatCool" case
		for Trho_key in Trho_keys:
			#counter 0: cooling file, 1: heating file
			cnt_heatCool = 0
			#tot_heatcool  = []

			# labels for title
			T_label = Trho_key.split('_')[1]
			T_label = int(float(T_label))
			rho_label1 = Trho_key.split('_')[-1].split('E')[0]
			rho_label2 = Trho_key.split('_')[-1].split('E')[1]
			rho_label1 = int((float(rho_label1)*10))
			rho_label2 = int(rho_label2)-1

			# loop over all files
			# equals one for "noheatCool" case
			for file in ntw[heatCool_key][Trho_key]:
				# counter over all heatcool mechanism in "mechanisms"
				cnt_mech = 0

				# loop over all heatcool mechanism in "mechanisms"
				for mech in mechanisms[cnt_heatCool]:
					# get index/column of heatcool mechanism
					col=heatcool_list[cnt_heatCool].index(mech)
					# read correct column from file
					cool = np.loadtxt(file, unpack=True, skiprows=1, usecols=(col,))
					time = np.arange(1,len(cool)+1)

					style_index = cnt_heatCool
					color_index = cnt_heatCool*len(mechanisms[0]) + cnt_mech
					color_label = mech

					if cnt_heatCool == 0:
						style_label= "Heating"
						# convert negative heating to cooling
						if cool[0] < 0:
							cool = -cool
							style_index = 1
							style_label= "Cooling"
						# total heating to total heat+cool
						if mech == "totheat":
							#tot_heatcool = cool[:]
							# no need to plot total heating
							continue
					else:
						style_label= "Cooling"
						# total cooling to total heat+cool
						if mech == "totcool":
							continue
							# # Check if total is net heating or cooling
							# if tot_heatcool[0] > cool[0]:
							# 	style_label = 'Heating'
							# 	style_index = 0

							#cool = tot_heatcool + cool[:]
							#color_label = "Total heating+cooling"

					line, = plt.semilogy(time,cool, ls=styles[style_index], c=colors[color_index] ,lw=5)
					color_handles.append(line)
					color_labels.append(color_label)

					cnt_mech = cnt_mech + 1
				cnt_heatCool = cnt_heatCool + 1
			cnt_Trho = cnt_Trho + 1
		cnt_ntw = cnt_ntw + 1

	# labels for title
	ntw_label = str(int(re.split('(\d+)',file.split('/')[-2])[1]))+ ' reactions'

	if subplot == False:
		# Create fake point for heating/cooling legend
		line, = plt.semilogy(-1,1e-10, ls=styles[0], c=colors[0] ,lw=5)
		style_handles.append(line)
		style_labels.append("Heating")
		line, = plt.semilogy(-1,1e-10, ls=styles[1], c=colors[0] ,lw=5)
		style_handles.append(line)
		style_labels.append("Cooling")

		plt.xlim(xmin=0,xmax=time[-1])
		plt.ylim(ymin=1e-35,ymax=1e0)
		plt.xlabel('time (hrs)')
		plt.ylabel(r'cooling (erg cm$^{-3}$ s$^{-1}$)')
		plt.title(r'Efficiency per heating/cooling for %s for initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm$^3$'\
		 %(ntw_label,T_label,rho_label1,rho_label2))
		legend1 = plt.legend(color_handles,color_labels,loc=2, \
		ncol=(len(mechanisms[0])+len(mechanisms[1])), fancybox=True, shadow=True)
		ax = plt.gca().add_artist(legend1)
		legend2 = plt.legend(style_handles,style_labels,loc=1,fancybox=True, shadow=True)

	if subplot == True:
		plt.xlim(xmin=0,xmax=time[-1])
		plt.ylim(ymin=1e-35,ymax=1e0)
		#plt.xlabel('time (hrs)')
		plt.ylabel(r'cooling (erg cm$^{-3}$ s$^{-1}$)')
		plt.title(r'Initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm$^3$'\
		 %(T_label,rho_label1,rho_label2))

		return color_handles,color_labels,ntw_label



def subplot_heatCool_efficiencies(network_dicts,heatCool_key,Trho_keys,mechanisms,fig):
	subplot=True

	# create handles needed for legends
	style_handles = []
	style_labels  = []

	for i in range(len(Trhos)):
		k=321+i	#nrow ncol nplot
		plt.subplot(k)
		color_handles,color_labels,ntw_label = plot_heatCool_efficiencies(network_dicts,heatCool_key,[Trhos[i]],mechanisms,subplot)
		if i>len(Trhos)-3:
			plt.xlabel('time (hrs)')

	fig.suptitle('Efficiency per heating/cooling for %s' %(ntw_label),fontsize=20)
	# Create fake point for heating/cooling legend
	line, = plt.semilogy(-1,1e-10, ls=styles[0], c=colors[0] ,lw=5)
	style_handles.append(line)
	style_labels.append("Heating")
	line, = plt.semilogy(-1,1e-10, ls=styles[1], c=colors[0] ,lw=5)
	style_handles.append(line)
	style_labels.append("Cooling")

	legend1 = fig.legend(color_handles,color_labels,loc="upper center", \
	ncol=(len(mechanisms[0])+len(mechanisms[1])),bbox_to_anchor=(0.5, 0.97), fancybox=True, shadow=True)
	ax = fig.gca().add_artist(legend1)
	legend2 = fig.legend(style_handles,style_labels,loc=1,bbox_to_anchor=(0.9, 0.97),fancybox=True, shadow=True)



def plot_elements(network_dicts,heatCool_key,Trho_keys,elements,subplot=False):

	# create handles needed for legends
	color_handles = []
	color_labels  = []
	#style_handles = []
	#style_labels  = []

	# counter for the network
	cnt_ntw = 0

	# loop over all networks
	# type = dictionary
	for ntw in network_dicts:
		# counter for T-rho combo's
		cnt_Trho = 0

		# loop over all T-rho combo's
		# equals one for "heatCool" case
		for Trho_key in Trho_keys:
			#counter for elements in "elements"
			cnt_elem = 0
			#tot_heatcool  = []

			# labels for title
			T_label = Trho_key.split('_')[1]
			T_label = int(float(T_label))
			rho_label1 = Trho_key.split('_')[-1].split('E')[0]
			rho_label2 = Trho_key.split('_')[-1].split('E')[1]
			rho_label1 = int((float(rho_label1)*10))
			rho_label2 = int(rho_label2)-1

			# loop over all files
			# equals one for "noheatCool" case
			for file in ntw[heatCool_key][Trho_key]:
				# skip files which do not include all heating/cooling
				ending = file.split('_')[-1]
				if "COMPTON.dat" not in ending:
					continue
				# dictionary with all {element1:[abundances],element2:[abundances],...}
				# also includes non-elements like e.g. "time"
				elem_frac = dict()
				with open(file, 'r') as f:
					first_line = f.readline().strip().split(" ")

				data=np.loadtxt(file, unpack=False, skiprows=1)

				# convert data to dictionary
				# counter for all columns
				cnt_col = 0
				for ii in first_line:

					elem_frac[ii] = data[:,cnt_col]
					cnt_col = cnt_col + 1

				style_index = 1 #'-'

				for el in elements:
					color_index = elements.index(el)
					color_label = el

					line, = plt.semilogy(elem_frac['time']/3600.,elem_frac[el], ls=styles[style_index], c=colors[color_index] ,lw=5)
				#	line, = plt.plot(elem_frac['time']/3600.,elem_frac[el], ls=styles[style_index], c=colors[color_index] ,lw=5)
					color_handles.append(line)
					color_labels.append(color_label)

	# labels for title
	ntw_label = str(int(re.split('(\d+)',file.split('/')[-2])[1]))+ ' reactions'

	if subplot == False:
		plt.xlim(xmin=0,xmax=elem_frac['time'][-1]/3600.)
		#plt.ylim(ymin=1e-35,ymax=1e0)
		plt.xlabel('time (hrs)')
		plt.ylabel('Mass fraction')
		plt.title(r'Elemental mass fractions for %s for initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm^3'\
		 %(ntw_label,T_label,rho_label1,rho_label2))
		legend1 = plt.legend(color_handles,color_labels,loc=2, \
		bbox_to_anchor=(1, 1),fancybox=True, shadow=True)
		ax = plt.gca().add_artist(legend1)
	else:
		plt.xlim(xmin=0,xmax=elem_frac['time'][-1]/3600.)
		#plt.ylim(ymin=1e-35,ymax=1e0)
		plt.ylabel('Mass fraction')
		plt.title(r'Initial T = %i K rho = $%i\cdot 10^{%i}$ g/cm^3'\
			%(T_label,rho_label1,rho_label2))

		return color_handles,color_labels,ntw_label

def subplot_elements(network_dicts,heatCool_key,Trho_keys,elements,fig):
	subplot = True

	for i in range(len(Trhos)):
		k=321+i	#nrow ncol nplot
		plt.subplot(k)
		color_handles,color_labels,ntw_label = plot_elements(network_dicts,heatCool_key,[Trhos[i]],elements,subplot)
		if i>len(Trhos)-3:
			plt.xlabel('time (hrs)')

	fig.suptitle('Elemental mass fractions for %s' %(ntw_label),fontsize=20)
	legend1 = fig.legend(color_handles,color_labels,loc="upper center", \
	ncol=len(elements),bbox_to_anchor=(0.5, 0.97), fancybox=True, shadow=True)
	ax = fig.gca().add_artist(legend1)


def plot_single_element(file,elements):
	# create handles needed for legends
	color_handles = []
	color_labels  = []

	elem_frac = dict()
	with open(file, 'r') as f:
		first_line = f.readline().strip().split(" ")
	data=np.loadtxt(file, unpack=False, skiprows=1)

	# convert data to dictionary
	# counter for all columns
	cnt_col = 0
	for ii in first_line:

		elem_frac[ii] = data[:,cnt_col]
		cnt_col = cnt_col + 1

	style_index = 1 #'-'

	for el in elements:
		color_index = elements.index(el)
		color_label = el

		line, = plt.semilogy(elem_frac['time']/3600.,elem_frac[el], ls=styles[style_index], c=colors[color_index] ,lw=5)
	#	line, = plt.plot(elem_frac['time']/3600.,elem_frac[el], ls=styles[style_index], c=colors[color_index] ,lw=5)
		color_handles.append(line)
		color_labels.append(color_label)

	plt.xlim(xmin=0,xmax=elem_frac['time'][-1]/3600.)
	#plt.ylim(ymin=1e-35,ymax=1e0)
	plt.xlabel('time (hrs)')
	plt.ylabel('Mass fraction')

	legend1 = plt.legend(color_handles,color_labels,loc=2, \
	fancybox=True, shadow=True)
	ax = plt.gca().add_artist(legend1)

# Choose which temperature-density combos you want for plotting
# NOT YET for random combo's, only for ascending in lists
def get_temp_rho_combos(N_temp,N_rho):
	list_combos = []
	for T_idx in range(N_temp):
		for rho_idx in range(N_rho):
			T_value = temp_list[T_idx]
			rho_value = rho_list[rho_idx]
			list_combos.append("T_"+ T_value +"_rho_"+rho_value)
	return list_combos

Trhos = get_temp_rho_combos(3,2)
# ============ CPU plotting =================================

plt.figure(0)
Trhos = get_temp_rho_combos(3,2)
plot_CPUtime_per_network(dictnames,"noheatCool",Trhos)
plt.show()
# plt.savefig('CPU_time')
# plt.figure(1)
# plot_CPUtime_per_network([dictnames[-1]],"noheatCool",Trhos)

# plt.figure(1)
# Trhos = get_temp_rho_combos(3,2)
# plot_CPUtime_per_network(dictnames,"heatCool",[Trhos[3]])

# Trhos = get_temp_rho_combos(3,2)
# for i in range(6):
# 	plt.figure(i)
# #Trhos = get_temp_rho_combos(3,2)
# 	plot_CPUtime_per_network(dictnames,"heatCool",[Trhos[i]],norm=True)

# plt.figure(5)
# plot_CPUtime_per_network([dictnames[1]],"heatCool",[Trhos[3]],norm=True)
#plt.savefig('CPU_time_heatcool')
#plt.show()
# plt.figure(2)
# Trhos = get_temp_rho_combos(3,2)
# plot_CPUtime_per_network(dictnames,"gammas",[Trhos[3]])

#plt.show()
# ============ Heating cooling efficiency plotting =================================

#Trhos = get_temp_rho_combos(3,2)
#plt.figure(3)
#plot_heatCool_efficiencies([dictnames[-1]],"heatCool_eff",[Trhos[0]],heatcool_relevant)


# fig = plt.figure(6)
# subplot_heatCool_efficiencies([dictnames[-1]],"heatCool_eff",Trhos,heatcool_relevant,fig)
# #plt.show()
# # fig.savefig('heatCool_eff_1800')
# fig = plt.figure(7)
# subplot_heatCool_efficiencies([dictnames[0]],"heatCool_eff",Trhos,heatcool_relevant,fig)
# plt.show()

# ============ Element abundance plotting =================================
#
elements_relevant = ['C','C+','O','O+','SI','SI+','FE','FE+']
#elements_relevant = ['H','H+','H2']#,'H-','H2','H2+','HE','E']#,'SI','SI+','FE','FE+']

# plt.figure(10)
# plot_elements([dictnames[-2]],"heatCool",[Trhos[2]],elements_relevant)
# plt.show()
# #
# # fig = plt.figure(10)
# # subplot_elements([dictnames[-2]],"heatCool",Trhos,elements_relevant,fig)
#
# # elements_relevant = ['H2O','HCN','SIO','CO','CN']
# # fig = plt.figure(11)
# # subplot_elements([dictnames[-1]],"heatCool",Trhos,elements_relevant,fig)
# fig = plt.figure(12)
#
# subplot_elements([dictnames[-1]],"heatCool",Trhos,elements_relevant,fig)
# #
#
# plt.show()

# fig = plt.figure(12)
# subplot_elements([dictnames[-1]],"heatCool",Trhos,elements_relevant,fig)
# plt.show()

# tests = ['./figs/T_0.2E+05_rho_0.1E-09_KIDA.dat', './figs/T_0.2E+05_rho_0.1E-09_Primordial3.dat']
# cnt_fig = 0
# for t in tests:
# 	for el in elements_relevant:
# 		plt.figure(cnt_fig)
# 		plot_single_element(t,[el])
# 		plt.savefig(str(cnt_fig))
# 		cnt_fig = cnt_fig +1
#plt.show()
