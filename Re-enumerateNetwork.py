import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import fileinput

##############################################################################

#renumerate reactions
fname = sys.argv[1].strip()
cnt = 1

for line in fileinput.input(fname,inplace=True):
    if "@" in line or "#" in line or line == "\n":
        print line,
    else:
        linesplit = line.strip().split(",")
        linelist = [str(cnt)] + linesplit[1:]
        newline = ",".join(linelist)
        print newline
        cnt = cnt + 1
