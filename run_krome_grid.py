import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from argparse import Namespace
import re
from contextlib import contextmanager
from shutil import copy
from pprint import pprint
import fileinput
import subprocess


dirs = Namespace(output=Path("/STER/jelsb/PhD/KROME/krome_models/output_gfe"),
                 build_in=Path("/home/jelsb/codes/krome/build_nucleation_all"),
                 build_out=Path("/home/jelsb/codes/krome/build_nucleation_gfe")
                 )

files = Namespace(output="clusters_gfe_",
                  exec_in="evolve_all_clusters_",
                  exec_out="evolve_clusters_gfe_",
                  make='Makefile',
                  slurm='runslurm.sh'
                  )

amount = Namespace(threads=21,
                   decimal=2,
                   timestep=5
                   )
key = Namespace(job="job-name=",
                out="output=",
                err="error=",
                workdir="workdir=",
                srun="srun"
                )
# print(f"{1:02}")


def replace_variables(input_file, input_file_number):
    for row in input_file:
        if 'path =' in row:
            row = re.sub(r'\".+\"', f'\"{dirs.output}/\"', row)
        if 'file_number =' in row:
            row = re.sub(r'\".+\"', f'\"{files.output}{input_file_number}.dat\"', row)
        if "dt =" in row:
            row = f"  dt = {amount.timestep} * secondsPerDay\n"
        yield row


@contextmanager
def change_dir(destination):
    "Context manager to change dir and return"
    try:
        cwd = os.getcwd()
        os.chdir(destination)
        yield
    finally:
        os.chdir(cwd)


def copy_files_in_different_dir():
    with change_dir(dirs.build_in):
        output_files = []
        for i in range(amount.threads):
            print(i)
            file_dir = copy(f"{files.exec_in}{i:02}.f90", dirs.build_out)
            output_files.append(file_dir)


def change_and_write_main_f90_file():
    input_files = list(dirs.build_in.glob(f"*{files.exec_in}*.f90"))
    for f_in in input_files:
        f_in_number = f_in.stem[-amount.decimal:]
        output_file = f"{dirs.build_out}/{files.exec_out}{f_in_number}.f90"
        output_path = f_in.parents[1].joinpath(output_file)
        with f_in.open('r') as ff_in:
            with output_path.open('w') as ff_out:
                print(output_path)
                for line in replace_variables(ff_in, f_in_number):
                    ff_out.write(line)


def change_makefile(i):
    makefile = f'{dirs.build_out}/{files.make}'
    with fileinput.input(makefile, inplace=True, backup='.bak') as f:
        for line in f:
            if "exec = " in line:
                line = re.sub(r'exec =.+', f'exec = {files.exec_out}{i:02}', line)
            print(line.rstrip())

def change_slurm_file(i):
    slurmfile = f'{dirs.build_out}/{files.slurm}'
    with fileinput.input(slurmfile, inplace=True, backup='.bak') as f:
        for line in f:
            if key.job in line:
                line = re.sub(f'{key.job}.+', f'{key.job}krome_grid_{i:02}', line)
            elif key.out in line:
                line = re.sub(f'{key.out}.+',
                              f'{key.out}{dirs.output}/stdout_{i:02}', line)
            elif key.err in line:
                line = re.sub(f'{key.err}.+',
                              f'{key.err}{dirs.output}/stderr_{i:02}', line)
            elif key.workdir in line:
                line = re.sub(f'{key.workdir}.+',
                              f'{key.workdir}{dirs.build_out}/', line)
            elif key.srun in line:
                line = re.sub(f'{key.srun}.+',
                              f'{key.srun} ./{files.exec_out}{i:02}', line)
            print(line.rstrip())

def run_makefile():
    with change_dir(dirs.build_out):
        subprocess.run('make')

def run_slurmfile():
    with change_dir(dirs.build_out):
        subprocess.run(f'sbatch {files.slurm}', shell=True)

if __name__== "__main__":
    change_and_write_main_f90_file()
    for i in range(amount.threads):
        change_makefile(i)
        change_slurm_file(i)
        run_makefile()
        run_slurmfile()
