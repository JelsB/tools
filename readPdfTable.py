from tabula import read_pdf
from pathlib import Path
import pandas as pd
import numpy as np

mendeleypath = "/home/jels/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/"
pdf = ('Lee et al. - 2015 - Dust in brown dwarfs and extra-solar planets IV. '
       'Assessing TiO2 and SiO nucleation for cloud formation modelling.pdf')
pdfpath = mendeleypath + pdf
# values = []
out_path = Path("/home/jels/tools/Thermochemistry/data/literature_input")
header = ['T (K)', 'S (J/(K mol))', 'H-H_298 (kJ/mol)', 'dHf (kJ/mol)',
          'energy (Hartree)', 'E_zpve (eV)']
i = 1
for page in range(10, 25):
    if page in [13, 15, 17, 19]:
        continue
    if page == 10:
        df = read_pdf(pdfpath, pages=page).replace(regex=r'–', value='-')
        energy = df['E0[Hartree]'].values[2:16]
        Ezpve = df['Ezpe[eV]'].values[2:16]
        energy_bromley = np.delete(energy, [2, 4, 6, 8])
        Ezpve_bromley = np.delete(Ezpve, [2, 4, 6, 8])
        continue

    Ti_idx = str(i)
    if i == 1:
        Ti_idx = ''
    species_name = 'Ti'+Ti_idx+'O'+str(i*2)#+'_input'
    out = out_path.joinpath(species_name).with_suffix('.therm')

    df = read_pdf(pdfpath, pages=page).replace(regex=r'–', value='-')
    df.dropna(axis=1, inplace=True)
    # remove first row
    dfout = df.iloc[1:, [0, 1, 3, 4]]
    # add extra empty column
    column_lenght = len(dfout.iloc[:, 0])
    dfout = dfout.assign(energy=pd.Series([energy_bromley[i-1]]+['']*(column_lenght-1)).values)
    dfout = dfout.assign(E_zpve=pd.Series([Ezpve_bromley[i-1]]+['']*(column_lenght-1)).values)
    # print(dfout)
    # write out csv file
    dfout.to_csv(out, header=header, index=False)

    i += 1
