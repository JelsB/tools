import sys
import pandas as pd
from argparse import Namespace
from random import uniform
from collections import OrderedDict

from bokeh.io import show
from bokeh.layouts import column, row, gridplot
from bokeh.models import CustomJS, ColumnDataSource, Range1d, Plot
from bokeh.models import LogColorMapper, BasicTicker, PrintfTickFormatter, ColorBar
from bokeh.plotting import Figure, figure
from bokeh.models.glyphs import ImageURL
from bokeh.resources import CDN
from bokeh.embed import autoload_static, components

from bokeh.sampledata.unemployment1948 import data

rho_values = [10**i * exp for i in range(-10, -6) for exp in range(1, 10)]
# rho_values = [10**(i + exp / 10) for i in range(-10, -6) for exp in range(10)]
# [abs(rho_values[idx] - rho_values[idx + 1])
#  for idx in range(len(rho_values) - 1)]
# rho_values = [i + exp / 10 for i in range(-10, -8) for exp in range(10)]

temp_values = list(range(500, 600, 10))
temp_keys = [f'{i:04d}' for i in temp_values]

rho_delta = [abs(rho_values[idx] - rho_values[idx + 1])
             for idx in range(len(rho_values) - 1)
             for _ in range(len(temp_values))
             ] + [0] * len(temp_values)
len(rho_delta)

rho_str = [f'10^{(i + exp / 10)}' for i in range(1, 3) for exp in range(10)]


def get_flux():
    return [uniform(0, 10) * 10**(uniform(6, 15)) for _ in range(len(rho_values))]


d = {key: get_flux() for key in temp_values}
# d['density'] = rho_values
d['density'] = rho_values
# d['delta_density'] = rho_delta

df = pd.DataFrame(d).set_index('density')
df.columns.name = 'temperature'

# reshape to 1D array or rates with a month and year for each row.
df = pd.DataFrame(df.stack(), columns=['flux']).reset_index()
len(df['flux'])
df['delta_rho'] = rho_delta
df['delta_rho'][35:50]

colors = ["#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2",
          "#dfccce", "#ddb7b1", "#cc7878", "#933b41", "#550b1d"]

mapper = LogColorMapper(palette=colors, low=df.flux.min(), high=df.flux.max())

TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom"

p = figure(x_range=(min(temp_values), max(temp_values)),
           y_range=(min(rho_values), max(rho_values)),
           y_axis_type="log",
           x_axis_location="below", plot_width=900, plot_height=800,
           tools=TOOLS, toolbar_location='below')
p.axis
# tooltips=[('grid point', '@temperature @density'), ('flux', '@flux%')])

# p.grid.grid_line_color = None
# p.axis.axis_line_color = None
# p.axis.major_tick_line_color = None
p.axis.major_label_text_font_size = "5pt"
p.axis.major_label_standoff = 0
# p.xaxis.major_label_orientation = pi / 3


p.rect(x='temperature', y="density", width=10, height='delta_rho',
       source=df,
       height_units='data',
       fill_color={'field': 'flux', 'transform': mapper},
       line_color=None)


# print(vars(f.nonselection_glyph))


color_bar = ColorBar(color_mapper=mapper, major_label_text_font_size="5pt",
                     label_standoff=6, border_line_color=None, location=(0, 0))
p.add_layout(color_bar, 'right')

show(p)
