import sys
import pandas as pd
from argparse import Namespace
from random import uniform
from collections import OrderedDict

from bokeh.io import show
from bokeh.layouts import column, row, gridplot
from bokeh.models import CustomJS, ColumnDataSource, Range1d, Plot, LogTicker
from bokeh.models import LogColorMapper, BasicTicker, PrintfTickFormatter, ColorBar
from bokeh.plotting import Figure, figure
from bokeh.models.glyphs import ImageURL
from bokeh.resources import CDN
from bokeh.embed import autoload_static, components
from bokeh.palettes import viridis

from bokeh.sampledata.unemployment1948 import data

# rho_min = [10**i *exp for i in range(1, 3) for exp in range(10)]
# rho_min = [10**(i + exp / 10) for i in range(1, 3) for exp in range(10)]
rho_min = [10**i * exp for i in range(-10, -6) for exp in range(1, 10)]

rho_max = rho_min[1:] + [1e-6]

temp_min = list(range(500, 600, 10))
temp_max = temp_min[1:] + [600]

long_rho_min = [rho for rho in rho_min for _ in range(len(temp_min))]
long_rho_max = [rho for rho in rho_max for _ in range(len(temp_max))]
long_temp_min = [temp for _ in range(len(rho_min)) for temp in temp_min]
long_temp_max = [temp for _ in range(len(rho_max)) for temp in temp_max]

flux = [uniform(0, 10) * 10**(uniform(6, 15))
        for _ in range(len(temp_min) * len(rho_min))]

colors = ["#75968f", "#a5bab7", "#c9d9d3", "#e2e2e2",
          "#dfccce", "#ddb7b1", "#cc7878", "#933b41", "#550b1d"]

mapper = LogColorMapper(palette=viridis(256), low=min(flux), high=max(flux))

TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom"

p = figure(x_range=(min(temp_min), max(temp_max)),
           y_range=(min(rho_min), max(rho_max)),
           y_axis_type="log",
           x_axis_location="below", plot_width=900, plot_height=800,
           tools=TOOLS, toolbar_location='below')
p.axis
# toolti    ps = [('grid point', '@temperature @density'), ('flux', '@flux%')])

p.grid.grid_line_color = None
# p.axis.axis_line_color = None
# p.axis.major_tick_line_color = None
p.axis.major_label_text_font_size = "5pt"
p.axis.major_label_standoff = 0
# p.xaxis.major_label_orientation = pi / 3

# p.quad(left=long_temp_min, right=long_temp_max, bottom=long_rho_min,
#         top=long_rho_max,
#         fill_color={'value': 2, 'transform': mapper},
#         line_color='red')

d = {'left': long_temp_min, 'right': long_temp_max,
     'bottom': long_rho_min, 'top': long_rho_max,
     'flux': flux}

df = pd.DataFrame(d)
p.quad(left='left', right='right', bottom='bottom',
       top='top',
       source=df,
       fill_color={'field': 'flux', 'transform': mapper},
       line_color='red')

# print(vars(f.nonselection_glyph))

color_bar = ColorBar(color_mapper=mapper, ticker=LogTicker(),
                     label_standoff=20,
                     location=(0, 0))

p.add_layout(color_bar, 'right')

show(p)
