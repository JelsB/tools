from PIL import Image
from pathlib import Path

# dir = '/home/jels/Dropbox/Apps/ShareLaTeX/Jels\'s Second Paper/monomer/Al2O3/timeloop/'
dir = '/home/jels/codes/de/assets/bokeh/figs/monomer/'
images = Path(f'{dir}').glob('*png')
rename = False
rescale_percent = 0.5

cnt = 0
tot = len(images)
for im in images:
    img = Image.open(im)
    heigth = int(img.size[0]*rescale_percent)
    width = int(img.size[1]*rescale_percent)
    img = img.resize((heigth, width))
    new_name = im
    if rename:
        new_name = im.with_name(f"{im.stem}_resized{im.suffix}")

    img.save(f'{new_name}')
    cnt += 1
    print(f'Done! ({cnt/tot})')
