import itertools
import json
from pathlib import Path
from argparse import Namespace
from collections import namedtuple

rate_info = Namespace(
                    format = '@format:idx,R,R,P,rate',
                    reversed_format = '@format:idx,R,P,P,rate',
                    rate_function = 'general_cluster_growth_rate',
                    reversed_rate_function = 'revKc_with_GFE'
                    )

files = Namespace(cluster_file = Path('clusters.json')
                  )

wanted_cluster = 'Al2O3'

#Generate parenthesized contents in string as pairs (level, contents)
def parenthetic_contents(string, parenteses):
    opened = parenteses[0]
    closed = parenteses[1]
    stack = []
    for i, c in enumerate(string):
        if c == opened:
            stack.append(i)
        elif c == closed and stack:
            start = stack.pop()
            yield (len(stack), string[start + 1: i])


#get content in parenteses
def get_parenthetic_contents(string, parenteses):
    return list(parenthetic_contents(string, parenteses))


def make_cluster_list(cluster_layout, all_cluster_sizes):
    parentic_list = get_parenthetic_contents(cluster_layout, '()')
    cluster_list = []
    for i in all_cluster_sizes:
        cluster = cluster_layout
        for tup in parentic_list:
            str_in = tup[1]
            str_out = str_in.replace('x', f'{i}')
            str_out = str(eval(str_out))

            if str_out == '1':
                str_out = ''

            cluster = cluster.replace(f'({str_in})', str_out)
        cluster_list.append(cluster)

    return cluster_list


def make_cluster_network(cluster_combos, cluster_list, monomer, rate_info):
    cnt = 1
    for combo in cluster_combos:
        idx_r1 = combo[0]
        idx_r2 = combo[1]
        idx_p = sum(combo)
        r1 = cluster_list[idx_r1-1]
        r2 = cluster_list[idx_r2-1]
        p = cluster_list[idx_p-1]
        rate = f'{rate_info.rate_function}(idx_{monomer},{idx_r1},{idx_r2},Tgas)'
        rate_reversed = (f'{rate} * {rate_info.reversed_rate_function}'
                        f'(Tgas, [idx_{p}], [idx_{r1},idx_{r2}])')

        line_forward = f'{rate_info.format}\n'
        line_forward += f'{cnt},{r1},{r2},{p},{rate}\n\n'

        line_reversed = f'{rate_info.reversed_format}\n'
        line_reversed += f'{cnt+1},{p},{r1},{r2},{rate_reversed}\n\n'

        cnt +=2

        yield line_forward
        yield line_reversed

def read_json(input):
    "returns dict of json file"
    with input.open('r') as f:
        clusters = json.load(f)

    return clusters

def get_wanted_cluster(wanted_cluster, input_file):
    clusters = read_json(input_file)
    cluster_dict = clusters[wanted_cluster]
    cluster = namedtuple("Cluster", cluster_dict.keys())(*cluster_dict.values())

    return cluster

cluster = get_wanted_cluster(wanted_cluster, files.cluster_file)
all_cluster_sizes = list(range(1, cluster.max_size+1))
cluster_list = make_cluster_list(cluster.layout, all_cluster_sizes)
cluster_combos = [combo for combo in
                  itertools.combinations_with_replacement(all_cluster_sizes, 2)
                  if sum(combo) <= cluster.max_size]

output_file = Path(f'{cluster.monomer}_cluster.ntw')
with output_file.open('w') as f:
    for line in make_cluster_network(cluster_combos, cluster_list,
                                     cluster.monomer, rate_info):
        f.write(line)
